#ifndef BATTLESHIP_APLAYER_H
#define BATTLESHIP_APLAYER_H

#include "BattleField.h"


class BattleShipPlayer
{
public:
	virtual bool IsReadyForShot()const = 0;
	virtual bool ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt) = 0;
	virtual bool InitializeForPlay() = 0;
	virtual bool IsLose()const = 0;
	virtual bool EndGame() = 0;
	virtual bool GetEnemyCellAt(const Position & position, CellState & outCell)const = 0;
	virtual unsigned int GetColumnCount()const = 0;
	virtual unsigned int GetRowCount()const = 0;

	virtual ~BattleShipPlayer(){};

	virtual bool ReceiveShot(Position & position, CellState & outCellState) = 0;
};

#endif //BATTLESHIP_APLAYER_H