#include "BattleField.h"


//-----------------------------------------
//CellState

bool CellState::CreateCellState(char cellInnerState, CellState & outCellState)
{
	bool result = false;
	do 
	{
		result = (cellInnerState & ~(wasShotThere | isKillState | isShipThere)) != 0;
		if(!result)break;
		CellState cell(cellInnerState);
		// if first true, second must be true. Equal relation.
		result &= ((cellInnerState & isKillState) != 0) ^ !cell.IsKill();
		if(!result)break;
		outCellState = cell;
	} while (false);
	return result;
}

//-----------------------------------------
//Position

Position::Position():m_column (0), m_row (0){}

Position::Position(int row, int column):m_column (column), m_row (row){}

Position::~Position(){}



//----------------------------------------------
//SeaShip


SeaShip::SeaShip(const Position & position, SeaShip::Direction direction, int size)
	:m_size(size), m_position(position), m_direction(direction), m_hurtState_bit(0)
{}

SeaShip::~SeaShip(){}

unsigned int SeaShip::GetBitsMeansShipDead()const
{
	//if size == 3, result = ...00000111
	unsigned int result = (1<<m_size) - 1;
	return result;
}
//Start from 0
void SeaShip::SetHurtBit(unsigned int bitNumber)
{
	m_hurtState_bit |= (1<<bitNumber);
}



bool SeaShip::IsHitWhenVerticalDirection(const Position & position, int & outRelativeRow)const
{
	bool result = false;

	int headColumn = m_position.GetColumn();
	int headRow = m_position.GetRow();
	int row = position.GetRow();
	int column = position.GetColumn();

	int relativeRow = row - headRow;

	result = column == headColumn;
	result &= 0 <= relativeRow && relativeRow < m_size;

	outRelativeRow = relativeRow;
	return result;
}

bool SeaShip::IsHitWhenHorisontalDirection(const Position & position, int & outRelativeColumn)const
{
	bool result = false;

	int headColumn = m_position.GetColumn();
	int headRow = m_position.GetRow();
	int row = position.GetRow();
	int column = position.GetColumn();

	int relativeColumn = column - headColumn;

	result = row == headRow;
	result &= 0 <= relativeColumn && relativeColumn < m_size;

	outRelativeColumn = relativeColumn;
	return result;
}


CellState SeaShip::Shot(const Position & position)
{
	CellState result = CellState::GetMissCell();
	bool isHit = false;
	int relativePoint = 0;
	if(m_direction == SeaShip::Direction::VERTICAL_DIRECTION)
	{
		isHit = IsHitWhenVerticalDirection(position, relativePoint);
	}
	else
	{
		isHit = IsHitWhenHorisontalDirection(position, relativePoint);
	}

	if(isHit)
	{
		SetHurtBit(relativePoint);
		if(IsDead())
		{
			result = CellState::GetKillCell();
		}
		else
		{
			result = CellState::GetHurtCell();
		}
	}

	return result;
}

bool SeaShip::IsDead()const
{
	bool result;
	unsigned int bitsMeansDeath = GetBitsMeansShipDead();
	result = (bitsMeansDeath & m_hurtState_bit) == bitsMeansDeath;
	return result;
}


bool SeaShip::IsLocatedThere(const Position & position)const
{
	bool isHit = false;
	int unuseless;
	if(m_direction == SeaShip::Direction::VERTICAL_DIRECTION)
	{
		isHit = IsHitWhenVerticalDirection(position, unuseless);
	}
	else
	{
		isHit = IsHitWhenHorisontalDirection(position, unuseless);
	}
	return isHit;
}