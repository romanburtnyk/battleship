#include "BattleField.h"




ShotStatisticField::ShotStatisticField(unsigned int columnCount,unsigned int rowCount):m_rowCount(rowCount), m_columnCount(columnCount), 
	m_field(rowCount * columnCount, CellState::GetNoneCell())
{}

ShotStatisticField::~ShotStatisticField(){}

bool ShotStatisticField::SetPositionShotStatus(const Position & position, CellState cellState)
{
	int row = position.GetRow();
	int column = position.GetColumn();
	bool result = row < m_rowCount && column < m_columnCount;
	if (result)
	{
		if (cellState.IsKill())
		{
			SetAllKilledCells(position);
		}
		else
		{
			m_field[row * m_columnCount + column] = cellState;
		}
	}
	return result;
}

bool ShotStatisticField::GetPositionShotStatus(const Position & position,CellState & outCellState)const
{
	int row = position.GetRow();
	int column = position.GetColumn();
	bool result = row < m_rowCount && column < m_columnCount;
	if (result)
	{
		outCellState = m_field[row * m_columnCount + column];
	}
	return result;
}

void ShotStatisticField::SetAllKilledCells(const Position & position)
{
	SetAllKilledRecursice(position);
}

//If We there - we must mark cell as killed
void ShotStatisticField::SetAllKilledRecursice(const Position position)
{
	int row = position.GetRow();
	int column = position.GetColumn();
	CellState nextCell = CellState::GetNoneCell();
	m_field[row * m_columnCount + column] = CellState::GetKillCell();

	bool markLeftAsKilled = column > 0;
	if(markLeftAsKilled)
	{
		nextCell = m_field[row * m_columnCount + column - 1];
	}
	else
	{
		nextCell = CellState::GetNoneCell();
	}
	markLeftAsKilled = markLeftAsKilled && nextCell.IsHurt() && !nextCell.IsKill();
	if (markLeftAsKilled)
	{
		SetAllKilledRecursice(Position(row, column - 1));
	}

	bool markRightAsKilled = column < m_columnCount - 1;
	if(markRightAsKilled)
	{
		nextCell = m_field[row * m_columnCount + column + 1];
	}
	else
	{
		nextCell = CellState::GetNoneCell();
	}
	markRightAsKilled = markRightAsKilled && nextCell.IsHurt() && !nextCell.IsKill();
	if (markRightAsKilled)
	{
		SetAllKilledRecursice(Position(row, column + 1));
	}

	bool markUpAsKilled = row > 0;
	if(markUpAsKilled)
	{
		nextCell = m_field[(row - 1) * m_columnCount + column];
	}
	else
	{
		nextCell = CellState::GetNoneCell();
	}
	markUpAsKilled = markUpAsKilled && nextCell.IsHurt() && !nextCell.IsKill();
	if (markUpAsKilled)
	{
		SetAllKilledRecursice(Position(row - 1, column));
	}

	bool markDownAsKilled = row < m_rowCount - 1;
	if(markDownAsKilled)
	{
		nextCell = m_field[(row + 1) * m_columnCount + column];
	}
	else
	{
		nextCell = CellState::GetNoneCell();
	}
	markDownAsKilled = markDownAsKilled && nextCell.IsHurt() && !nextCell.IsKill();
	if (markDownAsKilled)
	{
		SetAllKilledRecursice(Position(row + 1, column));
	}
}
