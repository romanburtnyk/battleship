#include "ErrorContainer.h"

ErrorContainer * ErrorContainer::GetErrorContainer()
{
	if(m_errorContainer == 0)
		m_errorContainer = new ErrorContainer();
	return m_errorContainer;
}

void ErrorContainer::SetErrorText(QString & text)
{
	m_mutex.lock();
	m_errorText = text;
	m_mutex.unlock();
}
void ErrorContainer::GetErrorText(QString & text)
{
	m_mutex.lock();
	text = m_errorText;
	m_mutex.unlock();
}

ErrorContainer::ErrorContainer(){};