#ifndef ERROR_CONTAINER_H
#define ERROR_CONTAINER_H

#include <string>
#include <QString>
#include <QMutex>

class ErrorContainer
{
public:
	static ErrorContainer * GetErrorContainer();

	void SetErrorText(QString & text);
	void GetErrorText(QString & text);
	~ErrorContainer();
private:
	ErrorContainer();
	static ErrorContainer * m_errorContainer;
	QString m_errorText;
	QMutex m_mutex;
};



#endif//ERROR_CONTAINER_H