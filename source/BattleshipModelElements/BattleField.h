#ifndef BATTLESHIP_FIELDS_H
#define BATTLESHIP_FIELDS_H


#include <vector>
#include <list>
#include <cassert>


//TODO: All flags
class CellState
{
public:
	CellState():m_stateFlag(0) {}
	static CellState GetNoneCell()		{return CellState(0);}
	static CellState GetMissCell()		{return CellState(wasShotThere);}
	static CellState GetOnlyShipCell()	{return CellState(isShipThere);}
	static CellState GetHurtCell()		{return CellState(isShipThere | wasShotThere);}
	static CellState GetKillCell()		{return CellState(isShipThere | wasShotThere | isKillState);}
	//Deserialize
	static bool CreateCellState(char cellInnerState, CellState & outCellState);



	bool IsHurt(){return (m_stateFlag & (wasShotThere | isShipThere)) == (wasShotThere | isShipThere);}
	bool IsKill(){return (m_stateFlag & (wasShotThere | isShipThere | isKillState)) == (wasShotThere | isShipThere | isKillState);}
	bool IsShip(){return (m_stateFlag & isShipThere) != 0;}
	bool IsNone(){return m_stateFlag == 0;}
	bool IsMiss(){return m_stateFlag == wasShotThere;}
	//Serialize
	char GetInnerState(){return (char)m_stateFlag;}
private:
	typedef enum
	{
		wasShotThere = 0x1,
		isShipThere = 0x2,
		isKillState = 0x4
	}StateFlags;
	CellState(int state):m_stateFlag(state){}
	int m_stateFlag;
};


class Position
{
public:
	Position();
	Position(int row, int column);
	~Position();

	int GetRow()const
	{return m_row;}
	int GetColumn()const
	{return m_column;}
	void SetRow(int value)
	{m_row = value;}
	void SetColumn(int value)
	{m_column = value;}
private:
	int m_row;
	int m_column;
};


class SeaShip
{
public:
	typedef enum
	{
		VERTICAL_DIRECTION = 0, HORIZONTAL_DIRECTION
	}Direction;


	SeaShip(const Position & position, SeaShip::Direction direction, int size);
	~SeaShip();
	CellState Shot(const Position & position);
	bool IsLocatedThere(const Position & position)const;
	bool IsDead()const;

	unsigned int GetSize()const{return m_size;}
	Position GetPosition()const{return m_position;}
	SeaShip::Direction GetDirection()const{return m_direction;}
private:
	Position m_position;
	SeaShip::Direction m_direction;
	unsigned int m_size;
	unsigned int m_hurtState_bit;

	void SetHurtBit(unsigned int bitNumber);
	unsigned int GetBitsMeansShipDead()const;
	bool IsHitWhenHorisontalDirection(const Position & position, int & outRelativeColumn)const;
	bool IsHitWhenVerticalDirection(const Position & position, int & outRelativeRow)const;
};


class ShotStatisticField
{
public:

	typedef std::vector<CellState> CellStateCollection;


	ShotStatisticField(unsigned int columnCount,unsigned int rowCount);
	~ShotStatisticField();

	unsigned int GetColumnCount()const{return m_columnCount;}
	unsigned int GetRowCount()const{return m_rowCount;}

	bool GetPositionShotStatus(const Position & position, CellState & outCellState)const;
	bool SetPositionShotStatus(const Position & position, CellState positionStatus);
private:
	CellStateCollection m_field;
	unsigned int m_columnCount;
	unsigned int m_rowCount;

	void SetAllKilledCells(const Position & position);
	void SetAllKilledRecursice(const Position position);
};

class ShipAllocationField
{
public:

	typedef std::list<SeaShip> SeaShipCollection;

	ShipAllocationField(unsigned int columnCount,unsigned int rowCount);
	virtual ~ShipAllocationField(void);

	bool AddShip(SeaShip ship);
	bool RemoveShip(const Position & position);
	bool IsAllShipDead()const;
	bool ValidateBattlefield()const;

	CellState Shot(const Position & position);
	bool IsAnyShipThere(const Position & position)const;

	unsigned int GetColumnCount()const{return m_columnCount;}
	unsigned int GetRowCount()const{return m_rowCount;}

	void Serialize(std::string & outBuffer);
	static bool Deserialize(std::string & inBuffer, ShipAllocationField & outShipField);

private:

	typedef std::vector<bool> ShipsLocationCache;


	SeaShipCollection m_ships;
	unsigned int m_columnCount;
	unsigned int m_rowCount;
	ShipsLocationCache m_shipsCache;


	static bool DeserializeShips(const int * buffer, int shipCount, ShipAllocationField & outfield);

	void UpdateCache();

	void ShipAllocationField::ResetCache();
	//Think that ship located right
	void ShipAllocationField::UpdateCacheWhenHorisontalDirection(const Position & position, unsigned int size);

	void ShipAllocationField::UpdateCacheWhenVerticalDirection(const Position & position, unsigned int size);
	bool ShipAllocationField::IsAnyShipThereWithIgnoreInvalidCoordinates(const Position & position)const;

	bool CheckShipAlocationWhenHorisontalDirection(const Position & shipPosition, unsigned int size)const;
	bool CheckShipAlocationWhenVerticalDirection(const Position & shipPosition, unsigned int size)const;

};

#endif //BATTLESHIP_FIELDS_H