#include "BattleField.h"
#include <QtEndian>
#include <QDebug>

typedef std::list<SeaShip>::iterator ShipIterator;
typedef std::list<SeaShip>::const_iterator ConstShipIterator;


ShipAllocationField::ShipAllocationField(unsigned int columnCount, unsigned int rowCount)
	:m_columnCount(columnCount), m_rowCount(rowCount),
	m_shipsCache(rowCount * columnCount)
{}

ShipAllocationField::~ShipAllocationField(){}

bool ShipAllocationField::AddShip(SeaShip ship)
{
	bool result = false;
	Position shipPosition = ship.GetPosition();
	unsigned int size = ship.GetSize();
	SeaShip::Direction direction = ship.GetDirection();
	int row = shipPosition.GetRow();
	int column = shipPosition.GetColumn();
	bool isValidShipPosition = 0 <= row && row < m_rowCount;
	isValidShipPosition &= 0 <= column && column < m_columnCount;
	do 
	{
		if(!isValidShipPosition)
		{
			break;
		}

		if(direction == SeaShip::Direction::HORIZONTAL_DIRECTION)
		{
			isValidShipPosition = CheckShipAlocationWhenHorisontalDirection(shipPosition, size);
		}
		else
		{
			isValidShipPosition = CheckShipAlocationWhenVerticalDirection(shipPosition, size);
		}
	} while (false);

	if (isValidShipPosition)
	{
		m_ships.push_back(ship);
		UpdateCache();
		result = true;
	}
	return result;
}


bool ShipAllocationField::CheckShipAlocationWhenHorisontalDirection(const Position & shipPosition, unsigned int size)const
{
	int row = shipPosition.GetRow();
	int column = shipPosition.GetColumn();
	bool isValidShipPosition = column + size <= m_columnCount;
	if (isValidShipPosition)
	{
		for (int i = 0; i <= size + 1; i++)
		{
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row - 1, column + i - 1));
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row, column + i - 1));
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row + 1, column + i - 1));
			if(!isValidShipPosition)
			{
				break;
			}
		}
	}
	return isValidShipPosition;
}

bool ShipAllocationField::CheckShipAlocationWhenVerticalDirection(const Position & shipPosition, unsigned int size)const
{
	int row = shipPosition.GetRow();
	int column = shipPosition.GetColumn();
	bool isValidShipPosition = row + size <= m_rowCount;
	if (isValidShipPosition)
	{
		for (int i = 0; i <= size + 1; i++)
		{
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row + i - 1, column - 1));
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row + i - 1, column));
			isValidShipPosition &= !IsAnyShipThereWithIgnoreInvalidCoordinates(Position(row + i - 1, column + 1));
			if(!isValidShipPosition)
			{
				break;
			}
		}
	}
	return isValidShipPosition;
}


bool ShipAllocationField::RemoveShip(const Position & position)
{
	bool result = false;
	ShipIterator begin = m_ships.begin();
	ShipIterator end = m_ships.end();
	for (;begin != end; begin++)
	{
		if(begin->IsLocatedThere(position))
		{
			result = true;
			m_ships.erase(begin);
			UpdateCache();
			break;
		}
	}
	return result;
}

bool ShipAllocationField::IsAllShipDead()const
{
	bool result = false;
	bool isAnyShipAlive = false;
	ConstShipIterator begin = m_ships.cbegin();
	ConstShipIterator end = m_ships.cend();
	for (;begin != end; begin++)
	{
		if(!begin->IsDead())
		{
			isAnyShipAlive = true;
			break;
		}
	}
	result = !isAnyShipAlive;
	return result;
}

//Checks size of all ships and their count by game rule
bool ShipAllocationField::ValidateBattlefield()const
{
	bool result = false;
	bool validateFailed = false;

	int shipCountsBySize[4] = {0};
	ConstShipIterator begin = m_ships.cbegin();
	ConstShipIterator end = m_ships.cend();
	//counts elements by size
	while(begin != end)
	{
		unsigned int size = begin->GetSize();
		if(size < 1 || size > 4)
		{
			validateFailed = true;
			break;
		}
		shipCountsBySize[size - 1]++;
		begin++;
	}

	//Size=4 - 1 element, size=3 - 2 elements, size=2 - 3 elements, size=1 - 4 elements
	for (int i = 0; i < 4; i++)
	{
		if(shipCountsBySize[i] != 4 - i)
		{
			validateFailed = true;
			break;
		}
	}

	result = !validateFailed;
	return result;
}

CellState ShipAllocationField::Shot(const Position & position)
{
	CellState result = CellState::GetMissCell();
	ShipIterator begin = m_ships.begin();
	ShipIterator end = m_ships.end();
	for (;begin != end; begin++)
	{
		result = begin->Shot(position);
		if(result.IsHurt())
		{
			break;
		}
	}
	return result;
}


bool ShipAllocationField::IsAnyShipThere(const Position & position)const
{
	bool isAnyShipThere = IsAnyShipThereWithIgnoreInvalidCoordinates(position);
	return isAnyShipThere;
}

bool ShipAllocationField::IsAnyShipThereWithIgnoreInvalidCoordinates(const Position & position)const
{
	bool isAnyShipThere = false;
	int column = position.GetColumn();
	int row = position.GetRow();
	bool isValidCoordinates = (0 <= row) && row < m_rowCount;
	isValidCoordinates &=  (0 <= column) && column < m_columnCount;
	if(isValidCoordinates)
	{
		isAnyShipThere = m_shipsCache[row * m_columnCount + column];
	}
	return isAnyShipThere;
}

void ShipAllocationField::UpdateCache()
{
	ShipIterator begin = m_ships.begin();
	ShipIterator end = m_ships.end();
	ResetCache();
	for (;begin != end; begin++)
	{
		Position shipPosition = begin->GetPosition();
		unsigned int size = begin->GetSize();
		SeaShip::Direction direction = begin->GetDirection();
		if(direction == SeaShip::Direction::HORIZONTAL_DIRECTION)
		{
			UpdateCacheWhenHorisontalDirection(shipPosition, size);
		}
		else
		{
			UpdateCacheWhenVerticalDirection(shipPosition, size);
		}
	}
}


void ShipAllocationField::ResetCache()
{
	for (int i = 0; i < m_columnCount*m_rowCount; i++)
	{
		m_shipsCache[i] = false;
	}
}

//Think that ship located right
void ShipAllocationField::UpdateCacheWhenHorisontalDirection(const Position & position, unsigned int size)
{
	int column = position.GetColumn();
	int row = position.GetRow();
	assert(column + size <= m_columnCount);
	for (int i = 0; i < size; i++)
	{
		m_shipsCache[row * m_columnCount + (column + i)] = true;
	}
}


//Think that ship located right
void ShipAllocationField::UpdateCacheWhenVerticalDirection(const Position & position, unsigned int size)
{
	int column = position.GetColumn();
	int row = position.GetRow();
	assert(row + size <= m_rowCount);
	for (int i = 0; i < size; i++)
	{
		m_shipsCache[(row + i)* m_columnCount + column] = true;
	}
}

void ShipAllocationField::Serialize(std::string & outBuffer)
{
	int size = m_ships.size();
	int columns =  qToBigEndian(m_columnCount);
	int rows = qToBigEndian(m_rowCount);
	int bufferSize = (2 + 1 + size * 4)* (sizeof(int));
	char * buffer = new char[bufferSize];
	int * IntBuffer = (int *)static_cast<void *>(buffer);
	IntBuffer[0] = rows;
	IntBuffer[1] = columns;
	IntBuffer[2] = qToBigEndian(size);

	ConstShipIterator begin = m_ships.cbegin();
	ConstShipIterator end = m_ships.cend();
	int buffPosition = 3;
	while(begin != end)
	{
		int size = qToBigEndian(begin->GetSize());
		int direction =  qToBigEndian(static_cast<int>(begin->GetDirection()));
		int row =  qToBigEndian(begin->GetPosition().GetRow());
		int column =  qToBigEndian(begin->GetPosition().GetColumn());
		IntBuffer[buffPosition++] = row;
		IntBuffer[buffPosition++] = column;
		IntBuffer[buffPosition++] = direction;
		IntBuffer[buffPosition++] = size;
		begin++;
	}
	
	outBuffer.clear();
	outBuffer.append(buffer, bufferSize);
	delete buffer;
}

bool ShipAllocationField::Deserialize(std::string & inBuffer, ShipAllocationField & outShipField)
{
	const char * buffer = inBuffer.c_str();
	int rowCount = 0;
	int columnCount = 0;
	int shipCount = 0;
	const int * IntBuffer = (const int *)(void *)buffer;
	bool result = inBuffer.size() >= 3*sizeof(int);
	result &= (inBuffer.size() - 3*sizeof(int)) % (sizeof(int) * 4) == 0;
	do 
	{
		if(!result)
		{
			qDebug()<<"ship deserialize:bad size of message";
			break;
		}
		rowCount = qFromBigEndian(IntBuffer[0]);
		columnCount = qFromBigEndian(IntBuffer[1]);
		shipCount = qFromBigEndian(IntBuffer[2]);
		result = rowCount > 0 && columnCount > 0 && shipCount >= 0;
		if(!result)
		{
			qDebug()<<"ship deserialize:bad init data";
			break;
		}
		ShipAllocationField resultField(columnCount, rowCount); 
		result = inBuffer.size() == (3*sizeof(int) + shipCount*4*sizeof(int));
		if(!result)
		{
			qDebug()<<"ship deserialize:bad full size of message";
			break;
		}
		result = DeserializeShips(IntBuffer, shipCount, resultField);
		if(!result)
		{
			qDebug()<<"can't deserialize ships";
			break;
		}
		outShipField = ShipAllocationField(10, 10);
	} while (false);
	return result;
}

bool ShipAllocationField::DeserializeShips(const int * buffer, int shipCount, ShipAllocationField & outfield)
{
	bool result = false;
	for (int i = 0; i < shipCount; i++)
	{
		int size = qFromBigEndian(buffer[i]);
		int intDirection =  qToBigEndian(buffer[i + 1]);
		int row = qToBigEndian(buffer[i + 2]);
		int column = qToBigEndian(buffer[i + 3]);
		result = size > 0 && column >= 0 && row >= 0;
		SeaShip::Direction direction = static_cast<SeaShip::Direction>(intDirection);
		result = direction != SeaShip::HORIZONTAL_DIRECTION || direction != SeaShip::VERTICAL_DIRECTION;
		if(!result)
		{
			qDebug()<<"ship deserialize: geometry unknown";
			break;
		}
		Position shipHead(row, column);
		SeaShip ship(shipHead, direction, size);
		result = outfield.AddShip(ship);
		if(!result)
		{
			qDebug()<<"ship deserialize: can't add ship";
			break;
		}
	}
	return result;
}