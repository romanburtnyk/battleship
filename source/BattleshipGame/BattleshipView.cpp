#include "BattleshipView.h"
#include "QtWidgets/QMessageBox"

BattleshipView::BattleshipView(BattleshipModel & model, PositionSource & positionSource)
	: m_model(model), m_positionSource(positionSource), m_firstPlayerField(model), m_secondPlayerField(model),
	m_viewWindow(model.GetColumnCount(), model.GetRowCount(), &m_positionSource, &m_firstPlayerField, &m_secondPlayerField)
{

}

void BattleshipView::StartGame()
{
	m_viewWindow.show();
}
void BattleshipView::Redraw()
{
	m_viewWindow.update();
	Position firstPlayerPosition;
	CellState firstPlayerResult;
	Position secondPlayerPosition;
	CellState secondPlayerResult;
	m_model.GetFirstPlayerLastShot(firstPlayerPosition, firstPlayerResult);
	m_viewWindow.SetFirstPlayerLastShot(firstPlayerPosition, firstPlayerResult);
	m_model.GetSecondPlayerLastShot(secondPlayerPosition, secondPlayerResult);
	m_viewWindow.SetSecondPlayerLastShot(secondPlayerPosition, secondPlayerResult);

}
void BattleshipView::ShowError()
{
	QMessageBox::critical(&m_viewWindow, "Error", "Some error occurs");
	m_viewWindow.close();
}
void BattleshipView::EndGame()
{
	m_viewWindow.close();
}


void BattleshipView::ShowPlayerWin(bool firstPlayerWin)
{
	if(firstPlayerWin)
	{
		QMessageBox::about(&m_viewWindow, "Game over", "You WIN!!!!!!");
	}
	else
	{
		QMessageBox::about(&m_viewWindow, "Game over", "You LOSE!!!!!!");
	}
	m_viewWindow.close();
}

