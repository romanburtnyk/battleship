#include "RemotePlayerConfigureWindow.h"
#include "Players/NetworkClientPlayer.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>

RemotePlayerConfigureWindow::RemotePlayerConfigureWindow(QWidget *parent)
	: QWidget(parent), m_isCancel(true)
{
	ui.setupUi(this);
	connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(CancelWaiting()));
}

RemotePlayerConfigureWindow::~RemotePlayerConfigureWindow()
{

}


bool RemotePlayerConfigureWindow::GetRemotePlayer(ShipAllocationField & shipField, BattleShipPlayer *& outPlayer, bool & outIsSecondPlayerTurn)
{
	bool result = false;
	show();
	QTcpSocket * socket = new QTcpSocket();
	connect(socket, SIGNAL(readyRead()), this, SLOT(StopWaitForConnect()));
	socket->connectToHost("localhost", 6606);
	QApplication::exec();
	do 
	{
		if(m_isCancel)
		{
			socket->close();
			delete socket;
			break;
		}
		char isFirstPlay = 0;
		socket->waitForReadyRead();
		int readStatus = socket->read(&isFirstPlay, 1);
		result = readStatus != -1 && readStatus != 0;
		if (!result)
		{
			socket->close();
			delete socket;
			break;
		}
		outPlayer = new NetworkClientPlayer(shipField, socket);
		outIsSecondPlayerTurn = !isFirstPlay;
	} while (false);
	if (!result)
	{
		QMessageBox::about(this, "Connection", "Connection was failed");
	}
	return result;
}

void RemotePlayerConfigureWindow::StopWaitForConnect()
{
	m_isCancel = false;
	close();
}

void RemotePlayerConfigureWindow::CancelWaiting()
{
	m_isCancel = true;
	close();
}