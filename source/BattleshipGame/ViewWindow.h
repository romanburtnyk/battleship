#ifndef VIEWWINDOW_H
#define VIEWWINDOW_H

#include <QMainWindow>
#include "ui_ViewWindow.h"
#include "FirstPlayerCellStateAdapter.h"
#include "SecondPlayerCellStateAdapter.h"

class ViewWindow : public QMainWindow
{
	Q_OBJECT

public:
	ViewWindow(unsigned int columnCount, unsigned int rowCount, PositionSource * positionSource, 
		FirstPlayerCellStateAdapter * firstPlayerCells, SecondPlayerCellStateAdapter * secondPlayerCells, QWidget *parent = 0);
	~ViewWindow();

	void SetSecondPlayerLastShot(Position secondPlayerPosition, CellState secondPlayerResult);
	void SetFirstPlayerLastShot(Position firstPlayerPosition, CellState firstPlayerResult);
private:
	Ui::ViewWindow ui;
	PositionSource * m_positionSource;

private slots:
	void ClickShoot();
};

#endif // VIEWWINDOW_H
