#ifndef CELLSTATE_Battlefield_ADAPTER_H_
#define CELLSTATE_Battlefield_ADAPTER_H_

#include "CellStateGetterAdapter.h"

class CellStateBattlefieldAdapter:public ICellStateGetterAdapter
{
public:
	CellStateBattlefieldAdapter(ShipAllocationField & shipField);
	virtual ~CellStateBattlefieldAdapter();
	virtual CellState GetCellState(Position  & position);
private:
	ShipAllocationField & m_shipField;
};

#endif
