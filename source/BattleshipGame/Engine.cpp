#include "Engine.h"
#include "BattleshipModel/BattleshipModel.h"
#include "BattleshipModelElements/ErrorContainer.h"
#include "Players/PositionSourcePlayer.h"
#include "Players/DefaultComputerPlayer.h"
#include "Game.h"
#include "ShipFieldBuilderWindow.h"
#include "RemotePlayerConfigureWindow.h"

Engine::Engine()
{
}


Engine::~Engine()
{
}

bool Engine::Run()
{
	bool isExit = false;
	ShipFieldBuilderWindow::GameMode mode = ShipFieldBuilderWindow::COMPUTERPLAYER;
	ShipAllocationField * shipField = new ShipAllocationField(10, 10);
	while(true)
	{
		//Main window
		bool exit = !InitializeGameShip(shipField, mode);
		if(exit)
		{
			break;
		}
		do 
		{
			PositionSource positionSource;
			BattleShipPlayer * firstPlayer = 0;
			BattleShipPlayer * secondPlayer = 0;
			//Get Player window
			bool isSecondPlayerTurn = false;
			bool goToMainWindow = !GetPlayersByMode(mode, positionSource, shipField, firstPlayer, secondPlayer, isSecondPlayerTurn);
			if(goToMainWindow)
			{
				break;
			}

			//Start game
			BattleshipModel model(firstPlayer, secondPlayer, false, isSecondPlayerTurn);
			BattleshipView view(model, positionSource);
			Game game(model, view);
			game.Run();

			if(secondPlayer != 0)
			{
				delete secondPlayer;
			}
			if (firstPlayer != 0)
			{
				delete firstPlayer;
			}
		} while (false);
	}

	if(shipField != 0)
	{
		delete shipField;
	}
	return true;
}

bool Engine::InitializeGameShip(ShipAllocationField * shipField, ShipFieldBuilderWindow::GameMode & outMode)
{
	ShipFieldBuilderWindow w(shipField);
	w.show();
	QApplication::exec();
	bool isExit = w.IsExit();
	if(!isExit)
	{
		outMode = w.GetGameMode();
	}
	return !isExit;
}

bool Engine::GetPlayersByMode(ShipFieldBuilderWindow::GameMode mode, PositionSource & positionSource, ShipAllocationField * shipField, BattleShipPlayer *& outFirstPlayer, BattleShipPlayer *& outSecondPlayer, bool & outIsSecondPlayerTurn)
{
	bool result = false;
	BattleShipPlayer * firstPlayer = 0,* secondPlayer = 0;
	firstPlayer = new PositionSourcePlayer(*shipField, positionSource);
	switch(mode)
	{
	case ShipFieldBuilderWindow::COMPUTERPLAYER :
		firstPlayer = new PositionSourcePlayer(*shipField, positionSource);
		secondPlayer = new DefaulComputerPlayer(shipField->GetRowCount(), shipField->GetColumnCount());
		result = true;
		break;
	case ShipFieldBuilderWindow::REMOTEPLAYER:
		result = GetRemotePlayer(shipField, secondPlayer, outIsSecondPlayerTurn);
		break;
	}
	result &= firstPlayer !=0 && secondPlayer != 0;
	if(!result)
	{
		if(firstPlayer != 0)
			delete firstPlayer;
		if(secondPlayer != 0)
			delete secondPlayer;
	}
	else
	{
		outFirstPlayer = firstPlayer;
		outSecondPlayer = secondPlayer;
	}
	return result;
}

bool Engine::GetRemotePlayer(ShipAllocationField * shipField, BattleShipPlayer *& outSecondPlayer, bool & isSecondPlayerTurn)
{
	 RemotePlayerConfigureWindow w;
	 bool result = w.GetRemotePlayer(*shipField, outSecondPlayer, isSecondPlayerTurn);
	 result &= outSecondPlayer != 0;
	 return result;
}