#ifndef CELLSTATE_SECONDPLAYER_ADAPTER_H_
#define CELLSTATE_SECONDPLAYER_ADAPTER_H_

#include "CellStateGetterAdapter.h"
#include "BattleShipModel/BattleshipModel.h"

class SecondPlayerCellStateAdapter:public ICellStateGetterAdapter
{
public:
	SecondPlayerCellStateAdapter(BattleshipModel & model);
	virtual ~SecondPlayerCellStateAdapter();
	virtual CellState GetCellState(Position  & position);
private:
	BattleshipModel & m_model;
};

#endif//CELLSTATE_SECONDPLAYER_ADAPTER_H_
