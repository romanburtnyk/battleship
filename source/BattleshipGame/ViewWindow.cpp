#include "ViewWindow.h"

ViewWindow::ViewWindow(unsigned int columnCount, unsigned int rowCount, PositionSource * positionSource, 
	FirstPlayerCellStateAdapter * firstPlayerCells, 
	SecondPlayerCellStateAdapter * secondPlayerCells, QWidget *parent)
	: QMainWindow(parent), m_positionSource(positionSource)
{
	ui.setupUi(this);
	ui.m_firstPlayerField->Initialize(columnCount, rowCount, 0, firstPlayerCells);
	ui.m_secondPlayerField->Initialize(columnCount, rowCount, positionSource, secondPlayerCells);
	connect(ui.pushButton, SIGNAL(clicked()), this, SLOT(ClickShoot()));
}

ViewWindow::~ViewWindow()
{

}


void ViewWindow::ClickShoot()
{
	m_positionSource->SetPositionReady();
}


void ViewWindow::SetSecondPlayerLastShot(Position secondPlayerPosition, CellState secondPlayerResult)
{
	QString text = "";
	text += QString::number(secondPlayerPosition.GetRow() + 1);
	text += ", ";
	text +=  QString::number(secondPlayerPosition.GetColumn() + 1);
	ui.spPositionLabel->setText(text);
	text = "";
	if(secondPlayerResult.IsKill())
	{
		text = "Kill";
	}
	else
	{
		if(secondPlayerResult.IsMiss())
		{
			text = "Miss";
		}
		else
		{
			if(secondPlayerResult.IsHurt())
			{
				text = "Hurt";
			}
			else
			{
				text = "Unknown";
			}
		}
	}
	ui.spShotResult->setText(text);
}
void ViewWindow::SetFirstPlayerLastShot(Position firstPlayerPosition, CellState firstPlayerResult)
{
	QString text = "";
	text += QString::number(firstPlayerPosition.GetRow() + 1);
	text += ", ";
	text +=  QString::number(firstPlayerPosition.GetColumn() + 1);
	ui.fpPositionLabel->setText(text);
	text = "";
	if(firstPlayerResult.IsKill())
	{
		text = "Kill";
	}
	else
		if(firstPlayerResult.IsMiss())
		{
			text = "Miss";
		}
		else
			if(firstPlayerResult.IsHurt())
			{
				text = "Hurt";
			}
			else
			{
				text = "Unknown";
			}
			ui.fpShotResult->setText(text);
}