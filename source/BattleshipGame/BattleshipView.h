#ifndef BATTLESHIP_VIEW_H
#define BATTLESHIP_VIEW_H

#include "Players\PositionSource.h"
#include "BattleshipModel\BattleshipModel.h"
#include "ViewWindow.h"

class BattleshipView
{
public:
	BattleshipView(BattleshipModel & model, PositionSource & positionSource);
	void StartGame();
	void Redraw();
	void ShowError();
	void EndGame();
	void ShowPlayerWin(bool firstPlayerWin);
private:
	BattleshipModel & m_model;
	PositionSource & m_positionSource;
	FirstPlayerCellStateAdapter m_firstPlayerField;
	SecondPlayerCellStateAdapter m_secondPlayerField;
	ViewWindow m_viewWindow;

};

#endif//BATTLESHIP_VIEW_H
