#ifndef SHIPFIELDBUILDERWINDOW_H
#define SHIPFIELDBUILDERWINDOW_H

#include <QWidget>
#include "BattleshipModelElements\BattleField.h"
#include "Players/PositionSource.h"
#include "ui_ShipFieldBuilderWindow.h"
#include "CellStateBattlefieldAdapter.h"


class ShipFieldBuilderWindow : public QWidget
{
	Q_OBJECT

public:

	typedef enum
	{
		COMPUTERPLAYER, REMOTEPLAYER
	}GameMode;

	ShipFieldBuilderWindow(ShipAllocationField * field, QWidget *parent = 0);
	~ShipFieldBuilderWindow();
	bool IsExit()const;
	GameMode GetGameMode();
private:
	bool m_isExited;
	ShipAllocationField * m_field;
	CellStateBattlefieldAdapter * m_adapter;
	PositionSource m_positionSource;
	Ui::ShipFieldBuilderWindow ui;
	GameMode m_selectedGameMode;

	void AddShipBySize(unsigned int size);

	private slots:
	void AddSingleDeck();
	void AddDoubleDeck();
	void AddTripleDeck();
	void AddForthDeck();

	void DeleteShip();

	void OkClick();
	void CancelClick();
};

#endif // SHIPFIELDBUILDERWINDOW_H
