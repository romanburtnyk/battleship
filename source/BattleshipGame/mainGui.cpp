#include <iostream>
#include <exception>

#include "Engine.h"
#include "ViewWindow.h"
#include "QtWidgets/QApplication"
#include "QtWidgets/qmessagebox.h"
#include "CellStateBattlefieldAdapter.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);/*
	PositionSource * so = 0;
	ShipAllocationField field(10, 10);
	field.AddShip(SeaShip(Position(1, 1), SeaShip::HORIZONTAL_DIRECTION, 4));
	CellStateBattlefieldAdapter adap(field);
	ViewWindow w(10, 10, so, &adap);
	w.show();
	return QApplication::exec();*/
	Engine engine;
	try
	{
	engine.Run();
	}
	catch(std::exception & ex)
	{
		QMessageBox::critical(0, "Error", ex.what());
	}
	return 0;
}
