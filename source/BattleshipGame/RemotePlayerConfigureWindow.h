#ifndef REMOTEPLAYERCONFIGUREWINDOW_H
#define REMOTEPLAYERCONFIGUREWINDOW_H

#include <QWidget>
#include "ui_RemotePlayerConfigureWindow.h"
#include "BattleshipModelElements/BattleShipPlayer.h"
#include "BattleshipModelElements/BattleField.h"

class RemotePlayerConfigureWindow : public QWidget
{
	Q_OBJECT

public:
	RemotePlayerConfigureWindow(QWidget *parent = 0);
	~RemotePlayerConfigureWindow();
	bool GetRemotePlayer(ShipAllocationField & field, BattleShipPlayer *& outPlayer, bool & outIsSecondPlayerTurn);
private:
	Ui::RemotePlayerConfigureWindow ui;
	bool m_isCancel;
private slots:
	void StopWaitForConnect();
	void CancelWaiting();
};

#endif // REMOTEPLAYERCONFIGUREWINDOW_H
