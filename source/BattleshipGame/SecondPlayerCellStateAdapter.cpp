#include "SecondPlayerCellStateAdapter.h"

SecondPlayerCellStateAdapter::SecondPlayerCellStateAdapter(BattleshipModel & model):m_model(model)
{}

SecondPlayerCellStateAdapter::~SecondPlayerCellStateAdapter(){}

CellState SecondPlayerCellStateAdapter::GetCellState(Position & position)
{
	CellState state = CellState::GetNoneCell();
	m_model.SecondPlayerCellAt(position, state);
	return state;
}
