#include <QMouseEvent>
#include "BattleshipWidget.h"

//For nonError situation, when we don't Initialize, we has 1x1 drawing rectangle
BattleshipWidget::BattleshipWidget( QWidget *parent): QWidget(parent),
	m_rowCount(1), m_columnCount(1), m_cellstateGetter(0), m_positionSource(0)
{
	ui.setupUi(this);
}

BattleshipWidget::~BattleshipWidget()
{

}


void BattleshipWidget::Initialize(unsigned int columnCount, unsigned int rowCount, PositionSource * positionSource, ICellStateGetterAdapter * cellstateGetter)
{
	m_columnCount = columnCount;
	m_rowCount = rowCount;
	m_positionSource = positionSource;
	m_cellstateGetter = cellstateGetter;
}

void BattleshipWidget::paintEvent( QPaintEvent * event )
{
	QPainter painter(this);
	DrawBounds(painter);
	if(m_cellstateGetter != 0)
	{
		for(int i = 0; i < m_rowCount; i++)
		{
			for(int j = 0; j < m_columnCount; j++)
			{
				Position point(i, j);
				CellState cell = m_cellstateGetter->GetCellState(point);
				DrawCell(point, cell, painter);
			}
		}
	}
}


void BattleshipWidget::DrawCell(const Position & position, CellState cell, QPainter & painter)
{
	QBrush brush(Qt::BrushStyle::SolidPattern);
	QRect rectForCell = GetRectByWidgetSize(position);
	QRect rectForShip(rectForCell.left() + 3, rectForCell.top() + 3, rectForCell.width() - 6, rectForCell.height() - 6);
	if (cell.IsShip())
	{
		if(cell.IsKill())
		{
			QColor color(255, 0, 0);
			brush.setColor(color);
			painter.setBrush(brush);
		}
		else
		{
			QColor color(0, 255, 0);
			brush.setColor(color);
			painter.setBrush(brush);
		}
		painter.drawRect(rectForShip);
	}
	if (cell.IsHurt())
	{
		QColor color(0, 0, 0);
		brush.setColor(color);
		painter.setBrush(brush);
		painter.drawLine(rectForShip.left(), rectForShip.top(), rectForShip.right(), rectForShip.bottom());
		painter.drawLine(rectForShip.right(), rectForShip.top(), rectForShip.left(), rectForShip.bottom());
	}
	if (cell.IsMiss())
	{
		QColor color(0, 0, 255);
		brush.setColor(color);
		painter.setBrush(brush);
		painter.drawEllipse(rectForShip.left() + rectForShip.width()/4, rectForShip.top() + rectForShip.height()/4, rectForShip.width()/2, rectForShip.height()/2);
	}
	if (position.GetColumn() == m_chosenCell.GetColumn() && position.GetRow() == m_chosenCell.GetRow())
	{
		QColor color(200, 200, 0, 120);
		brush.setColor(color);
		painter.setBrush(brush);
		painter.drawRect(rectForCell);
	}
}

QRect BattleshipWidget::GetRectByWidgetSize(Position point)const
{
	QRect widgetRect= this->rect();
	int width = widgetRect.width() / m_columnCount;
	int height = widgetRect.height() / m_rowCount;
	QRect cellRect(point.GetColumn() * width, point.GetRow() * height, width, height);
	return cellRect;
}


void BattleshipWidget::DrawBounds(QPainter & painter)
{
	QBrush brush(Qt::BrushStyle::SolidPattern);
	QColor color(0, 0, 0);
	brush.setColor(color);
	painter.setBrush(brush);

	QRect widgetRect= this->rect();
	int cellWidth = widgetRect.width() / m_columnCount;
	int cellHeight = widgetRect.height() / m_rowCount;
	//Draw last line , that's why <= m_rowCount
	for(int i = 0; i <= m_rowCount; i++)
	{
		painter.drawLine(0, i * cellHeight, widgetRect.width(), i * cellHeight);
	}

	//Draw last line , that's why <= m_columnCount
	for(int j = 0; j <= m_columnCount; j++)
	{
		painter.drawLine(j * cellWidth, 0, j * cellWidth, widgetRect.height());
	}
}

//TODO:
void BattleshipWidget::mousePressEvent( QMouseEvent * event )
{
	if(m_positionSource != 0)
	{
		Position position = GetPositionByWindowCoordinates(event->pos());
		m_positionSource->SetPosition(position);
		m_chosenCell = position;
		update();
	}
}


Position BattleshipWidget::GetPositionByWindowCoordinates(QPoint point)
{
	QRect widgetRect= this->rect();
	int cellWidth = widgetRect.width() / m_columnCount;
	int cellHeight = widgetRect.height() / m_rowCount;
	int column = point.x() / cellWidth;
	int row = point.y() / cellHeight;
	Position result(row, column);
	return result;
}