#ifndef CELLSTATE_FIRSTPLAYER_ADAPTER_H_
#define CELLSTATE_FIRSTPLAYER_ADAPTER_H_

#include "CellStateGetterAdapter.h"
#include "BattleShipModel/BattleshipModel.h"

class FirstPlayerCellStateAdapter:public ICellStateGetterAdapter
{
public:
	FirstPlayerCellStateAdapter(BattleshipModel & model);
	virtual ~FirstPlayerCellStateAdapter();
	virtual CellState GetCellState(Position  & position);
private:
	BattleshipModel & m_model;
};

#endif//CELLSTATE_FIRSTPLAYER_ADAPTER_H_
