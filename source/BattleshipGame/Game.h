#ifndef BATTLESHIP_GAME_H
#define BATTLESHIP_GAME_H

#include <QtCore/QObject>
#include <QtCore/QTimer>

#include "BattleshipModel/BattleshipModel.h"
#include "BattleshipView.h"

class Game: public QObject
{
	Q_OBJECT
public:
	Game(BattleshipModel & model, BattleshipView & view);
	bool Run();
	~Game(void);
private slots:
	void UpdateAll();
private:
	static const int k_interval = 400;
	BattleshipModel & m_model;
	BattleshipView & m_view;
	QTimer m_timer;

	bool InitializeAll();
};



#endif//BATTLESHIP_ENGINE_H