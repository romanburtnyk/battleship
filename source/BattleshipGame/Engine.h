#ifndef BATTLESHIP_ENGINE_H
#define BATTLESHIP_ENGINE_H
#include "ShipFieldBuilderWindow.h"
#include "BattleshipModelElements/BattleShipPlayer.h"
class Engine
{
public:
	Engine(void);
	bool Run();
	~Engine(void);
private:
	bool InitializeGameShip(ShipAllocationField * shipField, ShipFieldBuilderWindow::GameMode & outMode);
	bool GetPlayersByMode(ShipFieldBuilderWindow::GameMode mode, PositionSource & positionSource, ShipAllocationField * shipField, BattleShipPlayer *& outFirstPlayer, BattleShipPlayer *& outSecondPlayer, bool & outIsSecondPlayerTurn);

	bool GetRemotePlayer(ShipAllocationField * shipField, BattleShipPlayer *& outSecondPlayer, bool & outIsSecondPlayerTurn);
};



#endif//BATTLESHIP_ENGINE_H