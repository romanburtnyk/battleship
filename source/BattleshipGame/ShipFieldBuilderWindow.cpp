#include "ShipFieldBuilderWindow.h"
#include <QtWidgets/QMessageBox>

ShipFieldBuilderWindow::ShipFieldBuilderWindow(ShipAllocationField * field, QWidget *parent)
	: QWidget(parent), m_field(field), m_isExited(true), m_selectedGameMode(COMPUTERPLAYER)

{
	ui.setupUi(this);
	m_adapter = new CellStateBattlefieldAdapter(*field);
	ui.m_battleField->Initialize(field->GetColumnCount(), field->GetRowCount(), &m_positionSource, m_adapter);
	connect(ui.singleDeckButton, SIGNAL(clicked()), this, SLOT(AddSingleDeck()));
	connect(ui.doubleDeckButton, SIGNAL(clicked()), this, SLOT(AddDoubleDeck()));
	connect(ui.tripleDeckButton, SIGNAL(clicked()), this, SLOT(AddTripleDeck()));
	connect(ui.forthDeckButton, SIGNAL(clicked()), this, SLOT(AddForthDeck()));
	connect(ui.deleteShipButton, SIGNAL(clicked()), this, SLOT(DeleteShip()));
	connect(ui.startButton, SIGNAL(clicked()), this, SLOT(OkClick()));
	connect(ui.CloseButton, SIGNAL(clicked()), this, SLOT(CancelClick()));
}

ShipFieldBuilderWindow::~ShipFieldBuilderWindow()
{
	delete m_adapter;
}

bool ShipFieldBuilderWindow::IsExit()const
{
	return m_isExited;
}

ShipFieldBuilderWindow::GameMode ShipFieldBuilderWindow::GetGameMode()
{
	return m_selectedGameMode;
}

void ShipFieldBuilderWindow::AddSingleDeck()
{
	AddShipBySize(1);
}
void ShipFieldBuilderWindow::AddDoubleDeck()
{
	AddShipBySize(2);
}
void ShipFieldBuilderWindow::AddTripleDeck()
{
	AddShipBySize(3);
}
void ShipFieldBuilderWindow::AddForthDeck()
{
	AddShipBySize(4);
}

void ShipFieldBuilderWindow::DeleteShip()
{
	m_positionSource.SetPositionReady();
	Position position;
	m_positionSource.GetPosition(position);
	m_field->RemoveShip(position);
	ui.m_battleField->update();
}

void ShipFieldBuilderWindow::AddShipBySize(unsigned int size)
{
	bool isHorisontal = ui.isHorisontalCheckBox->isChecked();
	SeaShip::Direction direction = SeaShip::VERTICAL_DIRECTION;
	if (isHorisontal)
	{
		direction = SeaShip::HORIZONTAL_DIRECTION;
	}
	m_positionSource.SetPositionReady();
	Position position;
	m_positionSource.GetPosition(position);
	SeaShip ship(position, direction, size);
	bool isAdded = m_field->AddShip(ship);
	if(!isAdded)
	{
		QMessageBox::about(this, "Add result", "Can't add ship there");
	}
	ui.m_battleField->update();
}

void ShipFieldBuilderWindow::OkClick()
{
	bool isShipFieldValid = m_field->ValidateBattlefield();
	if(!isShipFieldValid)
	{
		QMessageBox::about(this, "Can't start", "Ship allocation are not valid, please, put it by the rule 4-3-2-1");
	}
	else
	{
		m_isExited = false;
		switch(ui.gameModeComboBox->currentIndex())
		{
		case 1: m_selectedGameMode = REMOTEPLAYER;
			break;
		default: m_selectedGameMode = COMPUTERPLAYER;
		}
		close();
	}
}
void ShipFieldBuilderWindow::CancelClick()
{
	m_isExited = true;
	close();
}