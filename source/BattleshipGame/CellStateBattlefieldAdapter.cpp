#include "CellStateBattlefieldAdapter.h"

CellStateBattlefieldAdapter::CellStateBattlefieldAdapter(ShipAllocationField & shipField):m_shipField(shipField)
{

}

CellState CellStateBattlefieldAdapter::GetCellState(Position & position)
{
	CellState state = CellState::GetNoneCell();
	if(m_shipField.IsAnyShipThere(position))
	{
		state = CellState::GetOnlyShipCell();
	}
	return state;
}

CellStateBattlefieldAdapter::~CellStateBattlefieldAdapter(){}