#ifndef CELLSTATE_GETTER_ADAPTER_H_
#define CELLSTATE_GETTER_ADAPTER_H_

#include "BattleshipModelElements\BattleField.h"

class ICellStateGetterAdapter
{
public:
	virtual CellState GetCellState(Position  & position) = 0;
};

#endif
