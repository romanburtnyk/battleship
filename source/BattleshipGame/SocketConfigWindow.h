#ifndef SOCKETCONFIGWINDOW_H
#define SOCKETCONFIGWINDOW_H

#include <QWidget>
#include "ui_SocketConfigWindow.h"

class SocketConfigWindow : public QWidget
{
	Q_OBJECT

public:
	SocketConfigWindow(QWidget *parent = 0);
	~SocketConfigWindow();

private:
	Ui::SocketConfigWindow ui;
};

#endif // SOCKETCONFIGWINDOW_H
