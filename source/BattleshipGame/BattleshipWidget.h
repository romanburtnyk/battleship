#ifndef BATTLESHIPWIDGET_H
#define BATTLESHIPWIDGET_H

#include <QWidget>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>
#include "ui_BattleshipWidget.h"
#include "CellStateGetterAdapter.h"
#include "Players\PositionSource.h"

class BattleshipWidget : public QWidget
{
	Q_OBJECT

public:
	BattleshipWidget(QWidget *parent = 0);
	~BattleshipWidget();
	void Initialize(unsigned int columnCount, unsigned int rowCount, PositionSource * positionSource, ICellStateGetterAdapter * cellstateGetter);
protected:
	virtual void paintEvent( QPaintEvent * event );
	virtual void mousePressEvent ( QMouseEvent * event );
private:
	Ui::BattleshipWidget ui;
	unsigned int m_columnCount;
	unsigned int m_rowCount;
	PositionSource * m_positionSource;
	ICellStateGetterAdapter * m_cellstateGetter;
	Position m_chosenCell;


	void DrawCell(const Position & position, CellState cell, QPainter & painter);
	QRect GetRectByWidgetSize(Position point)const;
	void DrawBounds(QPainter & painter);
	Position GetPositionByWindowCoordinates(QPoint point);
};

#endif // BATTLESHIPWIDGET_H
