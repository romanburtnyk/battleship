#include "FirstPlayerCellStateAdapter.h"

FirstPlayerCellStateAdapter::FirstPlayerCellStateAdapter(BattleshipModel & model):m_model(model)
{}

FirstPlayerCellStateAdapter::~FirstPlayerCellStateAdapter(){}

CellState FirstPlayerCellStateAdapter::GetCellState(Position & position)
{
	CellState state = CellState::GetNoneCell();
	m_model.MainPlayerCellAt(position, state);
	return state;
}