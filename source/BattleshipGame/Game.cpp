
#include "Game.h"
#include <QtWidgets/QApplication>


Game::Game(BattleshipModel & model, BattleshipView & view):m_model(model), m_view(view), m_timer(this)
{
	m_timer.setSingleShot(false);
	m_timer.setInterval(k_interval);
	connect(&m_timer, SIGNAL(timeout()), this, SLOT(UpdateAll()));
}

Game::~Game(){};

bool Game::Run()
{
	bool result = InitializeAll();
	if(result)
	{
		m_timer.start();
		QApplication::exec();
		m_timer.stop();
	}
	m_model.EndGame();
	m_view.EndGame();
	return result;
}

bool Game::InitializeAll()
{
	bool isErrorRaise;
	m_view.StartGame();
	do 
	{
		isErrorRaise = !m_model.StartGame();
		if(isErrorRaise)
		{
			m_view.ShowError();
			break;
		}
	} while (false);
	return !isErrorRaise;
}

void Game::UpdateAll()
{
	bool errorRaise = m_model.Update();
	if(errorRaise)
	{
		m_timer.stop();
		m_view.ShowError();
	}
	else
	{
	m_view.Redraw();
	bool firstPlayerWin = false;
	if(m_model.IsAnyPlayerWin(firstPlayerWin))
	{
		m_timer.stop();
		m_view.ShowPlayerWin(firstPlayerWin);
	}
	}
}