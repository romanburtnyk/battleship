#include <iostream>
#include <QtCore/QCoreApplication>
#include "BattleshipServer.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	char command[1024];
	BattleshipServer server(6606);
	while (true)
	{
		std::cout<<"Enter command"<<std::endl;
		std::cin>>command;
		if(command[0] == 's')
		{
			server.Start();
		}
		if (command[0] == 'q')
		{
			server.Stop();
		}
		if(command[0] == 'e')
		{
			break;
		}
		if(command[0] == '?')
		{
			std::cout<<"Enter 's' to start server,\nEnter 'q' to stop server,\nEnter 'e' to exit"<<std::endl;
		}
	}
	return a.exec();
}
