#ifndef SERVER_WORKING_THREAD_H
#define SERVER_WORKING_THREAD_H

#include <QThread>

class BattleshipServer;

class ServerWorkingThread :public QThread
{
public:
	ServerWorkingThread(BattleshipServer & server);
protected:
	virtual void run();
private:
	BattleshipServer & m_server;
};

#endif//SERVER_WORKING_THREAD_H