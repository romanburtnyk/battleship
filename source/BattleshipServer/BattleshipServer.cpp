#include "BattleshipServer.h"
#include "NetworkServerPlayer.h"
#include <QtCore/QCoreApplication>

BattleshipServer::BattleshipServer(int port): m_port(port), m_isClosed(true), m_workingThread(*this)
{

}
bool BattleshipServer::Start()
{
	bool isStarted = false;
	if(m_isClosed)
	{
		m_isClosed = false;
		m_workingThread.start();
		QCoreApplication::exec();
	}
	return isStarted;
}
void BattleshipServer::Stop()
{
	m_isClosed = true;
	m_server.close();
	m_workingThread.wait();
}
//THIS IS THE HARDCODE
void BattleshipServer::Work()
{
	qDebug()<<"start work";
	QTcpServer m_server;
	m_server.listen(QHostAddress::Any, m_port);
	while(!m_isClosed)
	{
		m_server.waitForNewConnection();
		bool hasConnection = m_server.hasPendingConnections();
		do 
		{
			if(!hasConnection)
				break;
			QTcpSocket * socket = m_server.nextPendingConnection();
			if(socket == 0)
			{
				break;
			}
			ServeClient(socket);
		} while (false);
	}
	qDebug()<<"end work";
}

void BattleshipServer::ServeClient(QTcpSocket * client)
{
	qDebug()<<"new user connect";
	DeleteInvalidSockets();
	QTcpSocket * secondClient = GetSocketForGame();
	if(secondClient == 0)
	{
		qDebug()<<"Players not found";
		AddClient(client);
	}
	else
	{
		qDebug()<<"player found";
		StartGame(secondClient, client);
	}
}

QTcpSocket * BattleshipServer::GetSocketForGame()
{
	QTcpSocket * answer = 0;
	m_mutex.lock();
	if(m_playerSockets.size() > 0)
	{
		answer = m_playerSockets.front();
		m_playerSockets.pop_front();
	}
	m_mutex.unlock();
	return answer;
}

void BattleshipServer::DeleteInvalidSockets()
{
	m_mutex.lock();
	PlayersCollectionIterator begin = m_playerSockets.begin();
	PlayersCollectionIterator end = m_playerSockets.begin();
	while(begin != end)
	{
		PlayersCollectionIterator value = begin;
		++begin;
		if(!((*value)->isValid()))
		{
			m_playerSockets.remove(*value);
		}
	}
	m_mutex.unlock();
}

void BattleshipServer::AddClient(QTcpSocket * client)
{
	m_mutex.lock();
	m_playerSockets.push_back(client);
	m_mutex.unlock();
}

void BattleshipServer::StartGame(QTcpSocket * firstPlayer, QTcpSocket * secondPlayer)
{
	bool result = false;
	do 
	{
		char youStartFirst = 1;
		result = firstPlayer->write(&youStartFirst, 1);
		firstPlayer->waitForBytesWritten();
		if(!result)
		{
			qDebug()<<"Connection failed";
			break;
		}
		youStartFirst = 0;
		result = secondPlayer->write(&youStartFirst, 1);
		firstPlayer->waitForBytesWritten();
		if(!result)
		{
			qDebug()<<"Connection failed";
			break;
		}
		qDebug()<<"lets start game";
		NetworkServerPlayer * firstNetworkPlayer = new NetworkServerPlayer(firstPlayer);
		NetworkServerPlayer * secondNetworkPlayer = new NetworkServerPlayer(secondPlayer);
		BattleshipModel model(firstNetworkPlayer, secondNetworkPlayer, true);
		RunGame(model);
		delete firstNetworkPlayer;
		delete secondNetworkPlayer;
	} while (false);
	if(firstPlayer->isOpen())
	{
		firstPlayer->close();
	}
	if(secondPlayer->isOpen())
	{
		secondPlayer->close();
	}
	qDebug()<<"game ended";
}

void BattleshipServer::RunGame(BattleshipModel & model)
{
	bool result = false;
	bool AnyWin = false;
	bool firstWin = false;
	result = model.StartGame();
	while(true)
	{
		if(!result)
		{
			qDebug()<<"game failed";
			break;
		}
		result = model.Update();
		if(!result)
		{
			qDebug()<<"game failed";
			break;
		}
		AnyWin = model.IsAnyPlayerWin(firstWin);
		if(AnyWin)
		{
			qDebug()<<"First Win: "<<firstWin;
			break;
		}
	}
}