#ifndef BATTLESHIP_SERVER_H
#define BATTLESHIP_SERVER_H

#include <list>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMutex>

#include "BattleshipModel/BattleshipModel.h"
#include "ServerWorkingThread.h"


//BIG BIG HARDCODE AND UNASYNC CLASS!!!!!!
class BattleshipServer
{
public:
	typedef std::list<QTcpSocket *> PlayersCollection;
	typedef std::list<QTcpSocket *>::iterator PlayersCollectionIterator;

	BattleshipServer(int port);

	bool Start();
	void Stop();
private:
	friend ServerWorkingThread;
	bool m_isClosed;
	QTcpServer m_server;
	ServerWorkingThread m_workingThread;
	int m_port;
	PlayersCollection m_playerSockets;
	QMutex m_mutex;

	void Work();
	void StartGame(QTcpSocket * fistPlayer, QTcpSocket * secondPlayer);
	void DeleteInvalidSockets();
	QTcpSocket * GetSocketForGame();
	void AddClient(QTcpSocket * client);
	void RunGame(BattleshipModel & model);
	void ServeClient(QTcpSocket * client);
};


#endif//BATTLESHIP_SERVER_H
