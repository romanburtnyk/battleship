#include "ServerWorkingThread.h"
#include "BattleshipServer.h"


ServerWorkingThread::ServerWorkingThread(BattleshipServer & server):m_server(server)
{

}

void ServerWorkingThread::run()
{
	m_server.Work();
}
