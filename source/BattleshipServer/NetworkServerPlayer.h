#ifndef NETWORK_SERVER_PLAYER_H_
#define NETWORK_SERVER_PLAYER_H_


#include <QTcpSocket>
#include "BattleshipModelElements/BattleShipPlayer.h"
#include "Players/BattleshipNetworkSocket.h"

class NetworkServerPlayer: public BattleShipPlayer
{
public:
	NetworkServerPlayer(QTcpSocket * connectedServerSocket);
	virtual ~NetworkServerPlayer();
	virtual bool IsReadyForShot()const;
	//Called when in server call ReceiveShot
	virtual bool ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt);
	virtual bool InitializeForPlay();
	virtual bool IsLose()const;
	virtual bool EndGame();
	virtual bool GetEnemyCellAt(const Position & position, CellState & outCell)const;
	virtual unsigned int GetColumnCount()const;
	virtual unsigned int GetRowCount()const;
	//Called when in server call ShotAtPlayer
	virtual bool ReceiveShot(Position & position, CellState & outCellState);
private:
	ShotStatisticField m_shotField;
	ShipAllocationField m_thisPlayerShipField;
	QTcpSocket * m_serverSocket;
	BattleshipNetworkSocket m_battleshipSocket;
};

#endif //NETWORK_SERVER_PLAYER_H_