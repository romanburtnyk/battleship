#include <cstdlib>
#include <ctime>
#include "NetworkServerPlayer.h"
#include <QDebug>

NetworkServerPlayer::NetworkServerPlayer(QTcpSocket * connectedServerSocket)
	:m_thisPlayerShipField(1, 1), m_shotField(1, 1), m_battleshipSocket(*connectedServerSocket), 
	m_serverSocket(connectedServerSocket)
{
}

NetworkServerPlayer::~NetworkServerPlayer()
{
	EndGame();
}

unsigned int NetworkServerPlayer::GetColumnCount()const
{
	return m_shotField.GetColumnCount();
}

unsigned int NetworkServerPlayer::GetRowCount()const
{
	return m_shotField.GetRowCount();
}

bool NetworkServerPlayer::IsReadyForShot()const
{
	bool result = true;
	return result;
}


bool NetworkServerPlayer::InitializeForPlay()
{
	qDebug()<<"Receiving field";
	bool result = m_battleshipSocket.ReceiveShipField(m_thisPlayerShipField);
	if(result)
	{
		qDebug()<<"Receive success";
		m_shotField = ShotStatisticField(m_thisPlayerShipField.GetColumnCount(), m_thisPlayerShipField.GetRowCount());
	}
	else
	{
		qDebug()<<"Receive unsuccess";
	}
	return result;
}


bool NetworkServerPlayer::IsLose()const
{
	bool result = m_thisPlayerShipField.IsAllShipDead();
	return result;
} 

bool NetworkServerPlayer::ReceiveShot(Position & position, CellState & outCellState)
{
	bool result = position.GetColumn() < m_shotField.GetColumnCount();
	result &= position.GetRow() < m_shotField.GetRowCount();
	do 
	{
		if(!result)
		{
			break;
		}
		CellState state = CellState::GetNoneCell();
		result = m_battleshipSocket.SendPosition(position);
		if(!result)
		{
			break;
		}
		state = m_thisPlayerShipField.Shot(position);
		outCellState = state;
	} while (false);
	return result;
}

bool NetworkServerPlayer::ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt)
{
	bool result = false;
	bool isHurt = false;
	do 
	{
		CellState state = CellState::GetNoneCell();
		Position position;
		result = m_battleshipSocket.ReceivePosition(position);
		if(!result)break;
		result = enemyPlayer->ReceiveShot(position, state);
		if(!result)break;
		result = m_shotField.SetPositionShotStatus(position, state);
		if(!result)break;
		result = m_battleshipSocket.SendCellState(state, m_thisPlayerShipField.IsAllShipDead());
		if(!result)break;
		outIsHurt = state.IsHurt();
	} while (false);
	return result;
}

bool NetworkServerPlayer::GetEnemyCellAt(const Position & position, CellState & outCell)const
{
	CellState cell = CellState::GetNoneCell();
	bool result = m_shotField.GetPositionShotStatus(position, cell);
	if(result)
	{
		outCell = cell;
	}
	return result;
}

bool NetworkServerPlayer::EndGame()
{
	if(m_serverSocket->isOpen())
	{
		m_serverSocket->close();
	}
	return true;
}
