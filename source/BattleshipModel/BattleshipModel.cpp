#include "BattleshipModel.h"

#include "QThread"
#include "QMutex"
#include "QWaitCondition"
#include "Players/PositionSourcePlayer.h"

class AsyncUpdater : private QThread
{
public:
	AsyncUpdater(BattleshipModel & model):m_model(model), m_isErrorAppend(false){}
	bool NotifyUpdate();
	
	bool Start()
	{
		bool result = !m_model.m_isGameStopped & !m_isErrorAppend;
		if(result)
		{
			start();
		}
		return result;
	}
	bool Stop()
	{
		bool result = false;
		m_mutex.lock();
		m_model.m_isGameStopped = true;
		m_mutex.unlock();
		result = wait();
		return result;
	}

protected:
	virtual void run();
private:
	bool m_isErrorAppend;
	QMutex m_mutex;
	QWaitCondition m_condition;
	BattleshipModel & m_model;
};

bool AsyncUpdater::NotifyUpdate()
{
	m_condition.wakeOne();
	return !m_isErrorAppend;
}

void AsyncUpdater::run()
{
	//Stop condition
	if(!m_isErrorAppend)
	{
		while(!m_model.m_isGameStopped)
		{
			//Waiting for coordinates not locked mutex
			m_isErrorAppend |= !m_model.UpdateSync();
			if(m_isErrorAppend)break;
			m_mutex.lock();
			//provide exit before waiting 
			if(m_model.m_isGameStopped)
			{
				m_mutex.unlock();
				break;
			}
			m_condition.wait(&m_mutex);
			m_mutex.unlock();
		}
	}
}



BattleshipModel::BattleshipModel(BattleShipPlayer * mainPlayer, BattleShipPlayer * secondPlayer, bool isSycnUpdateMode, bool isSecondPlayerTurn)
	:
	m_isSecondPlayerTurn(isSecondPlayerTurn),
	m_isGameStopped(false),
	m_isSycnUpdateMode(isSycnUpdateMode),
	m_mainPlayer(mainPlayer), m_secondPlayer(secondPlayer),
	m_asyncUpdater(0)
{}

BattleshipModel::~BattleshipModel()
{
	if(m_asyncUpdater != 0)
	{
		delete m_asyncUpdater;
		m_asyncUpdater = 0;
	}
}

bool BattleshipModel::StartGame()
{
	bool result = false;
	do 
	{
		result = InitializeGame();
		if(!result) break;
		if(m_isSycnUpdateMode)
		{
			if(!result) break;
			m_isGameStopped = false;
		}
		else
		{
			if (m_asyncUpdater != 0)
				delete m_asyncUpdater;
			m_asyncUpdater = new AsyncUpdater(*this);
			result = m_asyncUpdater->Start();
		}
	} while (false);
	return result;
}

bool BattleshipModel::EndGame()
{
	bool result = false;
	do 
	{
		result = m_mainPlayer->EndGame();
		result &= m_secondPlayer->EndGame();
		if(m_isSycnUpdateMode)
		{
			result &= m_asyncUpdater->Stop();
		}
		else
		{
			m_isGameStopped = true;
		}
	} while (false);
	return result;
}



bool BattleshipModel::Update()
{
	bool result = false;
	if(m_isSycnUpdateMode)
	{
		result = UpdateSync();
	}
	else
	{
		if(m_asyncUpdater != 0)
		{
			result = m_asyncUpdater->NotifyUpdate();
		}
	}
	return false;
}

bool BattleshipModel::UpdateSync()
{
	bool result = false;
	if(m_isSecondPlayerTurn)
	{
		result = PlayerShooting(m_secondPlayer, m_mainPlayer);
	}
	else
	{
		result = PlayerShooting(m_mainPlayer, m_secondPlayer);
	}
	return result;
}

bool BattleshipModel::PlayerShooting(BattleShipPlayer * activePlayer, BattleShipPlayer * passivePlayer)
{
	bool withoutExceptions = true;
	if(activePlayer->IsReadyForShot()) 
	{
		bool isHurt = false;
		withoutExceptions = activePlayer->ShotAtPlayer(passivePlayer, isHurt);
		if (!isHurt)
		{
			m_isSecondPlayerTurn = !m_isSecondPlayerTurn;
		}
	}
	return withoutExceptions;
}

bool BattleshipModel::IsFirstPlayerTurn()const
{
	 return !m_isSecondPlayerTurn;
}


bool BattleshipModel::IsAnyPlayerWin(bool & outfirstPlayerWin)
{
	bool isAnyWin = false;
	bool isSecondLose = m_secondPlayer->IsLose();
	bool isFirstLose = m_mainPlayer->IsLose();
	if(isSecondLose | isFirstLose)
	{
		isAnyWin = true;
		outfirstPlayerWin = isSecondLose;
	}
	return isAnyWin;
}

unsigned int BattleshipModel::GetRowCount()const
{
	return m_mainPlayer->GetRowCount();
}
unsigned int BattleshipModel::GetColumnCount()const
{
	return m_mainPlayer->GetColumnCount();
}

bool BattleshipModel::MainPlayerCellAt(Position & position, CellState & outCellState)
{
	CellState state = CellState::GetNoneCell();
	bool result = false;
	result = m_secondPlayer->GetEnemyCellAt(position, state);

	
	if(result)
	{
		PositionSourcePlayer * guiPlayer = dynamic_cast<PositionSourcePlayer *> (m_mainPlayer);
		if(guiPlayer != 0 && state.IsNone() && guiPlayer->IsAnyShipThere(position))
		{
			state = CellState::GetOnlyShipCell();
		}
		outCellState = state;
	}
	return result;
}

bool BattleshipModel::SecondPlayerCellAt(Position & position, CellState & outCellState)
{
	CellState state = CellState::GetNoneCell();
	bool result = false;
	result = m_mainPlayer->GetEnemyCellAt(position, state);
	if(result)
	{
		outCellState = state;
	}
	return result;
}

bool BattleshipModel::IsGameInitializedRight()
{
	bool result = !m_isGameStopped;
	result &= m_mainPlayer != 0;
	result &= m_secondPlayer != 0;
	return result;
}

bool BattleshipModel::InitializeGame()
{
	bool result = false;
	do 
	{
		result = IsGameInitializedRight(); 
		if(!result)break;
		result &= m_mainPlayer->InitializeForPlay();
		result &= m_secondPlayer->InitializeForPlay();
	} while (false);
	return result;
}


bool BattleshipModel::GetFirstPlayerLastShot(Position & outPosition, CellState & outCellState)
{
	bool result;
	PositionSourcePlayer * guiPlayer = dynamic_cast<PositionSourcePlayer *> (m_mainPlayer);
	do 
	{
		result = guiPlayer != 0;
		if (!result)
		{
			break;
		}
		Position position;
		CellState cell;
		result = guiPlayer->GetFirstPlayerLastShot(position, cell);
		if (!result)
		{
			break;
		}
		outCellState = cell;
		outPosition = position;
	} while (false);
	return result;
}
bool BattleshipModel::GetSecondPlayerLastShot(Position & outPosition, CellState & outCellState)
{
	bool result;
	PositionSourcePlayer * guiPlayer = dynamic_cast<PositionSourcePlayer *> (m_mainPlayer);
	do 
	{
		result = guiPlayer != 0;
		if (!result)
		{
			break;
		}
		Position position;
		CellState cell;
		result = guiPlayer->GetSecondPlayerLastShot(position, cell);
		if (!result)
		{
			break;
		}
		outCellState = cell;
		outPosition = position;
	} while (false);
	return result;
}