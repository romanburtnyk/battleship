#ifndef BATTLESHIPMODEL_H
#define BATTLESHIPMODEL_H

#include "battleshipmodel_global.h"
#include "BattleshipModelElements\BattleShipPlayer.h"

class AsyncUpdater;

class BATTLESHIPMODEL_EXPORT BattleshipModel
{
public:
	BattleshipModel(BattleShipPlayer * mainPlayer, BattleShipPlayer * secondPlayer, bool isSycnUpdateMode = false, bool isSecondPlayerTurn = false);
	bool StartGame();
	bool EndGame();
	bool IsAnyPlayerWin(bool & outfirstPlayerWin);
	//when isSycnUpdateMode true, run UpdateSync. Else - work by m_asyncUpdater;
	bool Update();
	bool MainPlayerCellAt(Position & position, CellState & outCellState);
	bool SecondPlayerCellAt(Position & position, CellState & outCellState);
	unsigned int GetRowCount()const;
	unsigned int GetColumnCount()const;
	bool IsFirstPlayerTurn()const;

	bool GetFirstPlayerLastShot(Position & outPosition, CellState & outCellState);
	bool GetSecondPlayerLastShot(Position & outPosition, CellState & outCellState);

	~BattleshipModel();
private:
	friend AsyncUpdater;
	
	bool m_isSecondPlayerTurn;
	bool m_isSycnUpdateMode;
	bool m_isGameStopped;
	BattleShipPlayer * m_mainPlayer;
	BattleShipPlayer * m_secondPlayer;

	bool IsGameInitializedRight();
	bool PlayerShooting(BattleShipPlayer * activePlayer, BattleShipPlayer * passivePlayer);
	bool UpdateSync();
	bool InitializeGame();
	AsyncUpdater * m_asyncUpdater;
};

#endif // BATTLESHIPMODEL_H
