
#include <iostream>
#include <vector>
#include <QTcpServer>
#include <QTcpSocket>

#include <QtCore/QObject>
#include <QtCore/QMetaObject>
#include <QApplication>

class React : public QObject
{
	Q_OBJECT
public:
	void Run(QTcpServer & server)
	{
		connect(&server, SIGNAL(newConnection()), this, SLOT(NewConnect()));
		while(true);
	}
public slots:
	void NewConnect();
};

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTcpServer server;
	server.listen(QHostAddress::Any, 33003);
	React r;

	r.Run(server);
	return 0;
}

void React::NewConnect()
{
	std::cout<<"New connect";
}