#ifndef DEFAULT_COMPUTER_PLAYER_H_
#define DEFAULT_COMPUTER_PLAYER_H_



#include "BattleshipModelElements\BattleShipPlayer.h"

class DefaulComputerPlayer: public BattleShipPlayer
{
public:
	DefaulComputerPlayer(unsigned int rowCount, unsigned int columnCount);
	virtual ~DefaulComputerPlayer();
	virtual bool IsReadyForShot()const;
	virtual bool ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt);
	virtual bool InitializeForPlay();
	virtual bool IsLose()const;
	virtual bool EndGame();
	virtual bool GetEnemyCellAt(const Position & position, CellState & outCell)const;
	virtual unsigned int GetColumnCount()const;
	virtual unsigned int GetRowCount()const;

	virtual bool ReceiveShot(Position & position, CellState & outCellState);
private:
	ShotStatisticField m_shotField;
	ShipAllocationField m_shipField;

	void GenerateBattlefield();
	void GenerateShips(unsigned int shipSize, unsigned int shipCount);
protected:
	ShipAllocationField & GetShipField(){return m_shipField;}

	virtual bool DefaulComputerPlayer::GeneratePosition(Position & outposition);
};

#endif//DEFAULT_COMPUTER_PLAYER_H_