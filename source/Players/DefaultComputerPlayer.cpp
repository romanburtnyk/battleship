#include <cstdlib>
#include <ctime>
#include "DefaultComputerPlayer.h"

DefaulComputerPlayer::DefaulComputerPlayer(unsigned int rowCount, unsigned int columnCount):m_shipField(columnCount, rowCount),
	m_shotField(columnCount, rowCount)
{
	std::srand(std::time(0));
}

DefaulComputerPlayer::~DefaulComputerPlayer(){}

unsigned int DefaulComputerPlayer::GetColumnCount()const
{
	return m_shipField.GetColumnCount();
}

unsigned int DefaulComputerPlayer::GetRowCount()const
{
	return m_shipField.GetRowCount();
}

bool DefaulComputerPlayer::IsReadyForShot()const
{
	bool result = true;
	return result;
}


bool DefaulComputerPlayer::InitializeForPlay()
{
	GenerateBattlefield();
	return true;
}

bool DefaulComputerPlayer::IsLose()const
{
	bool isAllDead = m_shipField.IsAllShipDead();
	return isAllDead;
}

bool DefaulComputerPlayer::ReceiveShot(Position & position, CellState & outCellState)
{
	bool result = position.GetColumn() < m_shipField.GetColumnCount();
	result &= position.GetRow() < m_shipField.GetRowCount();
	if(result)
	{
		CellState state = m_shipField.Shot(position);
		outCellState = state;
	}
	return result;
}

bool DefaulComputerPlayer::ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt)
{
	bool result = false;
	bool isHurt = false;
	do 
	{
		CellState state = CellState::GetNoneCell();
		Position p;
		result = GeneratePosition(p);
		if(!result)break;
		result = enemyPlayer->ReceiveShot(p, state);
		if(!result)break;
		result = m_shotField.SetPositionShotStatus(p, state);
		if(!result)break;
		isHurt = state.IsHurt();
		outIsHurt = isHurt;
	} while (false);
	return result;
}

bool DefaulComputerPlayer::GeneratePosition(Position & outPosition)
{
	unsigned int columnCount = m_shipField.GetColumnCount();
	unsigned int rowCount = m_shipField.GetRowCount();
	bool isHitThere = true;
	Position result;
	while(isHitThere)
	{
		int column = rand()%columnCount;
		int row = rand()%rowCount;
		result = Position(row, column);
		CellState state = CellState::GetMissCell();
		m_shotField.GetPositionShotStatus(result, state);
		isHitThere = !state.IsNone();
	}
	outPosition = result;
	return true;
}

//TODO:
void DefaulComputerPlayer::GenerateBattlefield()
{
	for(int i = 0; i < 4; i++)
	{
		GenerateShips(4 - i, i + 1);
	}
}


void DefaulComputerPlayer::GenerateShips(unsigned int shipSize, unsigned int shipCount)
{
	unsigned int columnCount = m_shipField.GetColumnCount();
	unsigned int rowCount = m_shipField.GetRowCount();
	unsigned int generetatedShipCount = 0;
	while (true)
	{
		if(generetatedShipCount >= shipCount)
			break;
		int column = rand()%columnCount;
		int row = rand()%rowCount;
		Position shipPosition(row, column);
		bool isHorisontalDirection = rand() % 2 == 0;
		SeaShip ship(shipPosition, (isHorisontalDirection)?SeaShip::HORIZONTAL_DIRECTION:SeaShip::VERTICAL_DIRECTION, shipSize);
		bool successAdd = m_shipField.AddShip(ship);
		if(successAdd)
		{
			generetatedShipCount++;
		}
	}
}

bool DefaulComputerPlayer::GetEnemyCellAt(const Position & position, CellState & outCell)const
{
	CellState cell = CellState::GetNoneCell();
	bool result = m_shotField.GetPositionShotStatus(position, cell);
	if(result)
	{
		outCell = cell;
	}
	return result;
}

bool DefaulComputerPlayer::EndGame()
{
	return true;
}