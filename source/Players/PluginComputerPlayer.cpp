#include "PluginComputerPlayer.h"



PluginComputerPlayer::PluginComputerPlayer(unsigned int rowCount, unsigned int columnCount, QString libraryName)
	:DefaulComputerPlayer(rowCount, columnCount), m_library(libraryName)
{
}

PluginComputerPlayer::~PluginComputerPlayer()
{
	EndGame();
}

//Todo:
bool PluginComputerPlayer::InitializeForPlay()
{
	bool result = DefaulComputerPlayer::InitializeForPlay();
	do 
	{
		result = m_library.load();
		if(!result)break;
		m_createFunction = (CreateFn)m_library.resolve("InitializeComputerPlayer");
		m_destroyFunction= (DestroyFn)m_library.resolve("InitializeComputerPlayer");
		m_initFunction = (InitializeFn)m_library.resolve("InitializeComputerPlayer");
		m_getPositionFunction = (GetPosFn)m_library.resolve("InitializeComputerPlayer");
		result = m_createFunction != 0 && m_destroyFunction != 0 && m_initFunction != 0 && m_getPositionFunction != 0;
		if(!result)break;
		result = InitializeFromLibrary();
	} while (false);
	return result;
}

bool PluginComputerPlayer::EndGame()
{
	m_destroyFunction(m_handle);
	bool result = m_library.unload();
	return result;
}

bool PluginComputerPlayer::InitializeFromLibrary()
{
	m_handle = m_createFunction();
	bool result = m_initFunction(m_handle, &GetShipField());
	return result;
}

bool PluginComputerPlayer::GeneratePosition(Position & outPosition)
{
	Position position;
	bool result = m_getPositionFunction(m_handle, &position);
	return result;
}