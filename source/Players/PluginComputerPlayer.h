#ifndef PLUGIN_COMPUTER_PLAYER_H_
#define PLUGIN_COMPUTER_PLAYER_H_

#include <QLibrary> 
#include "BattleshipModelElements\BattleShipPlayer.h"
#include "DefaultComputerPlayer.h"
#include "PluginPlayerFuncPtrs.h"

class PluginComputerPlayer:public DefaulComputerPlayer
{
public:
	PluginComputerPlayer(unsigned int rowCount, unsigned int columnCount, QString libraryName);
	~PluginComputerPlayer();
	virtual bool InitializeForPlay();
	virtual bool EndGame();
protected:
	virtual bool GeneratePosition(Position & outPosition);
private:
	QLibrary m_library;
	ComputerPlayerHandle m_handle;
	CreateFn m_createFunction;
	DestroyFn m_destroyFunction;
	InitializeFn m_initFunction;
	GetPosFn m_getPositionFunction;

	bool InitializeFromLibrary();
};





#endif//INPUT_SOURCE_PLAYER_H_