#include "PositionSource.h"

PositionSource::PositionSource():m_isPositionReady(false)
{}
PositionSource::~PositionSource(){}


bool PositionSource::IsPositionReady()
{
	bool result = false;
	m_mutex.lock();
	result = m_isPositionReady;
	m_mutex.unlock();
	return result;
}

void PositionSource::SetPositionReady()
{
	m_mutex.lock();
	m_isPositionReady = true;
	m_mutex.unlock();
};

bool PositionSource::GetPosition(Position & outPosition)
{
	m_mutex.lock();
	bool result = m_isPositionReady;
	if(result)
	{
		outPosition = m_position;
		m_isPositionReady = false;
	}
	m_mutex.unlock();
	return result;
}

void PositionSource::SetPosition(Position position)
{
	m_mutex.lock();
	m_position = position;
	m_mutex.unlock();
}
