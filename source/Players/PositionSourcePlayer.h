#ifndef POSITION_SOURCE_PLAYER_H_
#define POSITION_SOURCE_PLAYER_H_

#include "BattleshipModelElements\BattleShipPlayer.h"
#include "PositionSource.h"

class PositionSourcePlayer:public BattleShipPlayer
{
public:
	PositionSourcePlayer(ShipAllocationField & shipField, PositionSource & positionSource);
	virtual ~PositionSourcePlayer();
	virtual bool IsReadyForShot()const;
	virtual bool ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt);
	virtual bool InitializeForPlay();
	virtual bool IsLose()const;
	virtual bool EndGame();
	virtual bool GetEnemyCellAt(const Position & position, CellState & outCell)const;
	virtual unsigned int GetColumnCount()const;
	virtual unsigned int GetRowCount()const;

	virtual bool ReceiveShot(Position & position, CellState & outCellState);

	bool IsAnyShipThere(Position & position)const;
	bool GetFirstPlayerLastShot(Position & outPosition, CellState & outCellState);
	bool GetSecondPlayerLastShot(Position & outPosition, CellState & outCellState);
private:
	ShotStatisticField m_shotField;
	ShipAllocationField & m_shipField;
	PositionSource & m_positionSource;
	Position m_firstPlayerLastPositionShot;
	Position m_secondPlayerLastPositionShot;

	CellState m_firstPlayerLastShotResult;
	CellState m_secondPlayerLastShotResult;
};





#endif//INPUT_SOURCE_PLAYER_H_