#include "PositionSourcePlayer.h"

PositionSourcePlayer::PositionSourcePlayer(ShipAllocationField & shipField, PositionSource & positionSource) 
	:m_shipField(shipField), m_positionSource(positionSource),
	m_shotField(shipField.GetColumnCount(), shipField.GetRowCount())
{
}

PositionSourcePlayer::~PositionSourcePlayer(){}

unsigned int PositionSourcePlayer::GetColumnCount()const
{
	return m_shipField.GetColumnCount();
}
	
unsigned int PositionSourcePlayer::GetRowCount()const
{
	return m_shipField.GetRowCount();
}


bool PositionSourcePlayer::IsReadyForShot()const
{
	bool result = m_positionSource.IsPositionReady();
	return result;
}


bool PositionSourcePlayer::InitializeForPlay()
{
	return true;
}

bool PositionSourcePlayer::IsLose()const
{
	bool isAllDead = m_shipField.IsAllShipDead();
	return isAllDead;
}

bool PositionSourcePlayer::ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt)
{
	bool result = false;
	bool isHurt = false;
	do 
	{
		CellState state = CellState::GetNoneCell();
		Position p;
		result = m_positionSource.GetPosition(p);
		if(!result)break;
		result = enemyPlayer->ReceiveShot(p, state);
		if(!result)break;
		result = m_shotField.SetPositionShotStatus(p, state);
		if(!result)break;
		isHurt = state.IsHurt();
		outIsHurt = isHurt;
		m_firstPlayerLastPositionShot = p;
		m_firstPlayerLastShotResult = state;
	} while (false);
	return result;
}

bool PositionSourcePlayer::IsAnyShipThere(Position & position)const
{
	bool isAnyShipThere = m_shipField.IsAnyShipThere(position);
	return isAnyShipThere;
}
bool PositionSourcePlayer::GetFirstPlayerLastShot(Position & outPosition, CellState & outCellState)
{
	bool result = !m_firstPlayerLastShotResult.IsNone();
	if (result)
	{
		outPosition = m_firstPlayerLastPositionShot;
		outCellState = m_firstPlayerLastShotResult;
	}
	return result;
}
bool PositionSourcePlayer::GetSecondPlayerLastShot(Position & outPosition, CellState & outCellState)
{
	bool result = !m_secondPlayerLastShotResult.IsNone();
	if (result)
	{
		outPosition = m_secondPlayerLastPositionShot;
		outCellState = m_secondPlayerLastShotResult;
	}
	return result;
}



bool PositionSourcePlayer::ReceiveShot(Position & position, CellState & outCellState)
{
	bool result = position.GetColumn() < m_shipField.GetColumnCount();
	result &= position.GetRow() < m_shipField.GetRowCount();
	if(result)
	{
		CellState state = m_shipField.Shot(position);
		outCellState = state;
		m_secondPlayerLastPositionShot = position;
		m_secondPlayerLastShotResult = state;
	}
	return result;
}

bool PositionSourcePlayer::GetEnemyCellAt(const Position & position, CellState & outCell)const
{
	CellState cell = CellState::GetNoneCell();
	bool result = m_shotField.GetPositionShotStatus(position, cell);
	if(result)
	{
		outCell = cell;
	}
	return result;
}

bool PositionSourcePlayer::EndGame()
{
	return true;
}