#include "BattleshipModelElements\BattleShipPlayer.h"

#ifndef NETWORK_CLIENT_PLAYER_H_
#define NETWORK_CLIENT_PLAYER_H_


#include <QTcpSocket>
#include "BattleshipModelElements\BattleShipPlayer.h"
#include "BattleshipNetworkSocket.h"

class NetworkClientPlayer: public BattleShipPlayer
{
public:
	NetworkClientPlayer(ShipAllocationField & m_shipMainPlayerField, QTcpSocket * connectedServerSocket);
	virtual ~NetworkClientPlayer();
	virtual bool IsReadyForShot()const;
	//Called when in server call ReceiveShot
	virtual bool ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt);
	virtual bool InitializeForPlay();
	virtual bool IsLose()const;
	virtual bool EndGame();
	virtual bool GetEnemyCellAt(const Position & position, CellState & outCell)const;
	virtual unsigned int GetColumnCount()const;
	virtual unsigned int GetRowCount()const;

	//Called when in server call ShotAtPlayer
	virtual bool ReceiveShot(Position & position, CellState & outCellState);
private:
	bool m_isLose;
	ShotStatisticField m_shotField;
	ShipAllocationField & m_mainPlayerShipField;
	QTcpSocket * m_serverSocket;
	BattleshipNetworkSocket m_battleshipSocket;
};

#endif //NETWORK_CLIENT_PLAYER_H_