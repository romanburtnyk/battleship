#include <cstdlib>
#include <ctime>
#include "NetworkClientPlayer.h"

NetworkClientPlayer::NetworkClientPlayer(ShipAllocationField & mainPlayerShipField, QTcpSocket * serverSocket)
	:m_mainPlayerShipField(mainPlayerShipField), m_battleshipSocket(*serverSocket), 
	m_serverSocket(serverSocket), m_shotField(mainPlayerShipField.GetColumnCount(), mainPlayerShipField.GetRowCount()),
	m_isLose(false)
{
}

NetworkClientPlayer::~NetworkClientPlayer()
{
	if (m_serverSocket != 0)
	{
		if(m_serverSocket->isOpen())
		{
			m_serverSocket->close();
		}
		delete m_serverSocket;
	}
}

unsigned int NetworkClientPlayer::GetColumnCount()const
{
	return m_shotField.GetColumnCount();
}

unsigned int NetworkClientPlayer::GetRowCount()const
{
	return m_shotField.GetRowCount();
}

bool NetworkClientPlayer::IsReadyForShot()const
{
	bool result = true;
	return result;
}


bool NetworkClientPlayer::InitializeForPlay()
{
	bool result = m_battleshipSocket.SendShipField(m_mainPlayerShipField);
	return result;
}


bool NetworkClientPlayer::IsLose()const
{
	return m_isLose;
} 

bool NetworkClientPlayer::ReceiveShot(Position & position, CellState & outCellState)
{
	bool result = position.GetColumn() < m_shotField.GetColumnCount();
	result &= position.GetRow() < m_shotField.GetRowCount();
	do 
	{
		if(!result)
		{
			break;
		}
		CellState state = CellState::GetNoneCell();
		result = m_battleshipSocket.SendPosition(position);
		if(!result)
		{
			break;
		}
		result = m_battleshipSocket.ReceiveCellState(state, m_isLose);
		if(!result)
		{
			break;
		}
		outCellState = state;
	} while (false);
	return result;
}

bool NetworkClientPlayer::ShotAtPlayer(BattleShipPlayer * enemyPlayer, bool & outIsHurt)
{
	bool result = false;
	bool isHurt = false;
	do
	{
		CellState state = CellState::GetNoneCell();
		Position position;
		result = m_battleshipSocket.ReceivePosition(position);
		if(!result)break;
		result = enemyPlayer->ReceiveShot(position, state);
		if(!result)break;
		result = m_shotField.SetPositionShotStatus(position, state);
		if(!result)break;
		outIsHurt = state.IsHurt();
	} while (false);
	return result;
}

bool NetworkClientPlayer::GetEnemyCellAt(const Position & position, CellState & outCell)const
{
	CellState cell = CellState::GetNoneCell();
	bool result = m_shotField.GetPositionShotStatus(position, cell);
	if(result)
	{
		outCell = cell;
	}
	return result;
}

bool NetworkClientPlayer::EndGame()
{
	if(m_serverSocket->isOpen())
	{
		m_serverSocket->close();
	}
	return true;
}
