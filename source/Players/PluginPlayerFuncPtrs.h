#ifndef PLUGINPLAYER_FUNCPTR_H
#define PLUGINPLAYER_FUNCPTR_H

#include "BattleshipModelElements/BattleField.h"

struct ComputerPlayerHandle
{
	void * handle;
};

typedef ComputerPlayerHandle (*CreateFn)() ;
typedef void (*DestroyFn)(ComputerPlayerHandle handle) ;
typedef bool (*InitializeFn)(ComputerPlayerHandle handle, void * field) ;
typedef bool (*GetPosFn)(ComputerPlayerHandle handle, void * outPosition) ;



#endif // PLUGINPLAYER_FUNCPTR_H
