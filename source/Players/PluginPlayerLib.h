#ifndef PLUGINPLAYER_GLOBAL_H
#define PLUGINPLAYER_GLOBAL_H

#include <QtCore/qglobal.h>

#include "BattleField.h"

#ifdef PLUGINPLAYER_LIB
# define PLUGINPLAYER_EXPORT Q_DECL_EXPORT
#else
# define PLUGINPLAYER_EXPORT Q_DECL_IMPORT
#endif

extern "C"
{
	PLUGINPLAYER_EXPORT ComputerPlayerHandle CreateComputerPlayer();
	PLUGINPLAYER_EXPORT void DestroyComputerPlayer(ComputerPlayerHandle handle);
	PLUGINPLAYER_EXPORT void InitializeComputerPlayer(ComputerPlayerHandle handle, void * field);
	PLUGINPLAYER_EXPORT bool GetPosition(ComputerPlayerHandle handle, void * outPosition);
};

#endif // BATTLESHIPMODEL_GLOBAL_H
