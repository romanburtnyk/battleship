#ifndef POSITION_SOURCE_H_
#define POSITION_SOURCE_H_

#include "BattleshipModelElements\BattleField.h"
#include <qmutex.h>

class PositionSource
{
public:
	PositionSource();
	~PositionSource();
	bool GetPosition(Position & outPosition);
	bool IsPositionReady();
	void SetPosition(Position position);
	void SetPositionReady();


private:
	Position m_position;
	bool m_isPositionReady;
	QMutex m_mutex;
};


#endif