#include "BattleshipNetworkSocket.h"

BattleshipNetworkSocket::BattleshipNetworkSocket(QTcpSocket & socket):m_socket(socket)
{

}


bool BattleshipNetworkSocket::ReceiveCellState(CellState & outCellState, bool & outAllShipDead)
{
	bool result = false;
	do
	{
		char receiveData[2];
		char isWinByte = 0;
		char cellInnerState = 0;
		result = ReceiveAll(receiveData, 2);
		if(!result)
		{
			break;
		}
		isWinByte = receiveData[0];
		cellInnerState = receiveData[1];
		CellState cell;
		result = CellState::CreateCellState(cellInnerState, cell);
		if(!result)
		{
			break;
		}
		outCellState = cell;
		outAllShipDead = isWinByte == 1;
	}while(false);
	return result;
}

bool BattleshipNetworkSocket::SendCellState(CellState state, bool allShipDead)
{
	char receiveData[2];
	bool result = false;
	do 
	{
		receiveData[0] = (char)allShipDead;
		receiveData[1] = state.GetInnerState();
		result = SendAll(receiveData, 2);
	} while (false);
	return result;
}

bool BattleshipNetworkSocket::ReceivePosition(Position & outPosition)
{
	bool result = false;
	int receiveSize = sizeof(int)*2;
	char * receiveData = new char[receiveSize];
	do 
	{
		result = ReceiveAll(receiveData, receiveSize);
		if(!result)
		{
			break;
		}
		int row = qFromBigEndian(*(int *)receiveData);
		int column = qFromBigEndian(*(int *)(receiveData + 1));
		result = row >= 0 && column >= 0;
		if(!result)
		{
			break;
		}
		outPosition = Position(row, column);
	} while (false);
	delete [] receiveData;
	return result;
}

bool BattleshipNetworkSocket::SendPosition(Position & outPosition)
{
	int receiveSize = sizeof(int)*2;
	char * receiveData = new char[receiveSize];
	int row = qToBigEndian(outPosition.GetRow());
	int column = qToBigEndian(outPosition.GetColumn());
	*((int *)receiveData) = row;
	*(((int *)receiveData) + 1) = column;
	bool result = SendAll(receiveData, receiveSize);
	return result;
}

bool BattleshipNetworkSocket::SendShipField(ShipAllocationField & shipField)
{
	std::string outBuffer;
	bool result = false;
	do 
	{
		shipField.Serialize(outBuffer);
		int size = outBuffer.size();
		size = qToBigEndian(size);
		bool result = SendAll((char*)&size, sizeof(int));
		if(!result)
		{
			break;
		}
		result = SendAll(outBuffer.c_str(), outBuffer.size());
	} while (false);
	return result;
}

bool BattleshipNetworkSocket::ReceiveShipField(ShipAllocationField & outShipField)
{
	std::string Buffer;
	bool result = false;
	char * outBuffer = 0;
	do 
	{
		int size = 0;
		result = ReceiveAll((char*)&size, sizeof(int));
		if(!result)
		{
			break;
		}
		size = qFromBigEndian(size);
		outBuffer = new char[size + 1];
		bool result = ReceiveAll(outBuffer, size);
		if(!result)
		{
			break;
		}
		std::string stringBuffer(outBuffer, size);
		ShipAllocationField field(1,1);
		result = ShipAllocationField::Deserialize(stringBuffer, field);
		if(!result)
		{
			break;
		}
		outShipField = field;
	} while (false);
	return result;
}

bool BattleshipNetworkSocket::SendAll(const char * outBuffer, unsigned int size)
{
	int sendedBytes = 0;
	bool result = false;
	while(true)
	{
		int sendStatus = m_socket.write(outBuffer + sendedBytes, size - sendedBytes);
		m_socket.waitForBytesWritten();
		if (sendStatus == 0 || sendStatus == -1)
		{
			result = false;
			break;
		}
		sendedBytes += sendStatus;
		if(sendedBytes >= size)
		{
			result = true;
			break;
		}
	}
	return result;
}

bool BattleshipNetworkSocket::ReceiveAll(char * outBuffer, unsigned int size)
{
	int receiveBytes = 0;
	bool result = false;
	while(true)
	{
		m_socket.waitForReadyRead();
		int sendStatus = m_socket.read(outBuffer + receiveBytes, size - receiveBytes);
		if (sendStatus == 0 || sendStatus == -1)
		{
			result = false;
			break;
		}
		receiveBytes += sendStatus;
		if(receiveBytes >= size)
		{
			result = true;
			break;
		}
	}
	return result;
}