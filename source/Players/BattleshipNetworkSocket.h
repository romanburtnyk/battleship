#ifndef BATTLESHIP_NETWORK_Socket_H
#define BATTLESHIP_NETWORK_Socket_H

#include <qtcpsocket.h>
#include <QtEndian>
#include "BattleshipModelElements/BattleField.h"

class BattleshipNetworkSocket
{
public:
	BattleshipNetworkSocket(QTcpSocket & socket);

	bool SendPosition(Position & position);
	bool ReceivePosition(Position & outPosition);

	bool SendCellState(CellState state, bool allShipDead);
	bool ReceiveCellState(CellState & outCellState, bool & outIsAllShipDead);

	bool SendShipField(ShipAllocationField & shipField);
	bool ReceiveShipField(ShipAllocationField & outShipField);

private:
	QTcpSocket & m_socket;
	bool SendAll(const char * outBuffer, unsigned int size);
	bool ReceiveAll(char * outBuffer, unsigned int size);
};

#endif