/********************************************************************************
** Form generated from reading UI file 'ShipFieldBuilderWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHIPFIELDBUILDERWINDOW_H
#define UI_SHIPFIELDBUILDERWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "battleshipwidget.h"

QT_BEGIN_NAMESPACE

class Ui_ShipFieldBuilderWindow
{
public:
    BattleshipWidget *m_battleField;
    QPushButton *doubleDeckButton;
    QPushButton *singleDeckButton;
    QPushButton *deleteShipButton;
    QPushButton *tripleDeckButton;
    QPushButton *forthDeckButton;
    QPushButton *startButton;
    QPushButton *CloseButton;
    QCheckBox *isHorisontalCheckBox;
    QGroupBox *groupBox;
    QComboBox *gameModeComboBox;

    void setupUi(QWidget *ShipFieldBuilderWindow)
    {
        if (ShipFieldBuilderWindow->objectName().isEmpty())
            ShipFieldBuilderWindow->setObjectName(QStringLiteral("ShipFieldBuilderWindow"));
        ShipFieldBuilderWindow->resize(770, 483);
        m_battleField = new BattleshipWidget(ShipFieldBuilderWindow);
        m_battleField->setObjectName(QStringLiteral("m_battleField"));
        m_battleField->setGeometry(QRect(0, 0, 481, 381));
        doubleDeckButton = new QPushButton(ShipFieldBuilderWindow);
        doubleDeckButton->setObjectName(QStringLiteral("doubleDeckButton"));
        doubleDeckButton->setGeometry(QRect(530, 50, 101, 23));
        singleDeckButton = new QPushButton(ShipFieldBuilderWindow);
        singleDeckButton->setObjectName(QStringLiteral("singleDeckButton"));
        singleDeckButton->setGeometry(QRect(530, 20, 101, 23));
        deleteShipButton = new QPushButton(ShipFieldBuilderWindow);
        deleteShipButton->setObjectName(QStringLiteral("deleteShipButton"));
        deleteShipButton->setGeometry(QRect(530, 160, 101, 23));
        tripleDeckButton = new QPushButton(ShipFieldBuilderWindow);
        tripleDeckButton->setObjectName(QStringLiteral("tripleDeckButton"));
        tripleDeckButton->setGeometry(QRect(530, 80, 101, 23));
        forthDeckButton = new QPushButton(ShipFieldBuilderWindow);
        forthDeckButton->setObjectName(QStringLiteral("forthDeckButton"));
        forthDeckButton->setGeometry(QRect(530, 110, 101, 23));
        startButton = new QPushButton(ShipFieldBuilderWindow);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setGeometry(QRect(530, 410, 91, 51));
        CloseButton = new QPushButton(ShipFieldBuilderWindow);
        CloseButton->setObjectName(QStringLiteral("CloseButton"));
        CloseButton->setGeometry(QRect(650, 408, 91, 51));
        isHorisontalCheckBox = new QCheckBox(ShipFieldBuilderWindow);
        isHorisontalCheckBox->setObjectName(QStringLiteral("isHorisontalCheckBox"));
        isHorisontalCheckBox->setGeometry(QRect(650, 70, 91, 17));
        groupBox = new QGroupBox(ShipFieldBuilderWindow);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(530, 240, 201, 111));
        gameModeComboBox = new QComboBox(groupBox);
        gameModeComboBox->setObjectName(QStringLiteral("gameModeComboBox"));
        gameModeComboBox->setGeometry(QRect(20, 40, 161, 22));

        retranslateUi(ShipFieldBuilderWindow);

        gameModeComboBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ShipFieldBuilderWindow);
    } // setupUi

    void retranslateUi(QWidget *ShipFieldBuilderWindow)
    {
        ShipFieldBuilderWindow->setWindowTitle(QApplication::translate("ShipFieldBuilderWindow", "ShipFieldBuilderWindow", 0));
        doubleDeckButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Set double-deck", 0));
        singleDeckButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Set single-deck", 0));
        deleteShipButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Delete ship", 0));
        tripleDeckButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Set triple-deck", 0));
        forthDeckButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Set forth-deck", 0));
        startButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Start Game", 0));
        CloseButton->setText(QApplication::translate("ShipFieldBuilderWindow", "Cancel", 0));
        isHorisontalCheckBox->setText(QApplication::translate("ShipFieldBuilderWindow", "Horisontal ship", 0));
        groupBox->setTitle(QApplication::translate("ShipFieldBuilderWindow", "Game mode", 0));
        gameModeComboBox->clear();
        gameModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("ShipFieldBuilderWindow", "One player", 0)
         << QApplication::translate("ShipFieldBuilderWindow", "Multiplayer", 0)
         << QString()
         << QString()
        );
    } // retranslateUi

};

namespace Ui {
    class ShipFieldBuilderWindow: public Ui_ShipFieldBuilderWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHIPFIELDBUILDERWINDOW_H
