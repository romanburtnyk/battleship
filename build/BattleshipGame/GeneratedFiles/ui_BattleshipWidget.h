/********************************************************************************
** Form generated from reading UI file 'BattleshipWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BATTLESHIPWIDGET_H
#define UI_BATTLESHIPWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BattleshipWidget
{
public:

    void setupUi(QWidget *BattleshipWidget)
    {
        if (BattleshipWidget->objectName().isEmpty())
            BattleshipWidget->setObjectName(QStringLiteral("BattleshipWidget"));
        BattleshipWidget->resize(348, 247);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        BattleshipWidget->setPalette(palette);

        retranslateUi(BattleshipWidget);

        QMetaObject::connectSlotsByName(BattleshipWidget);
    } // setupUi

    void retranslateUi(QWidget *BattleshipWidget)
    {
        BattleshipWidget->setWindowTitle(QApplication::translate("BattleshipWidget", "BattleshipWidget", 0));
    } // retranslateUi

};

namespace Ui {
    class BattleshipWidget: public Ui_BattleshipWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BATTLESHIPWIDGET_H
