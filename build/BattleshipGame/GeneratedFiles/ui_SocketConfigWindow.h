/********************************************************************************
** Form generated from reading UI file 'SocketConfigWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOCKETCONFIGWINDOW_H
#define UI_SOCKETCONFIGWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SocketConfigWindow
{
public:
    QLabel *label;
    QLabel *label_2;
    QTextEdit *textEdit;
    QPushButton *OkButton;
    QPushButton *cancelButton;
    QSpinBox *spinBox;

    void setupUi(QWidget *SocketConfigWindow)
    {
        if (SocketConfigWindow->objectName().isEmpty())
            SocketConfigWindow->setObjectName(QStringLiteral("SocketConfigWindow"));
        SocketConfigWindow->resize(373, 271);
        label = new QLabel(SocketConfigWindow);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 100, 71, 16));
        label_2 = new QLabel(SocketConfigWindow);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(90, 150, 71, 16));
        textEdit = new QTextEdit(SocketConfigWindow);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(170, 100, 104, 31));
        OkButton = new QPushButton(SocketConfigWindow);
        OkButton->setObjectName(QStringLiteral("OkButton"));
        OkButton->setGeometry(QRect(180, 230, 75, 23));
        cancelButton = new QPushButton(SocketConfigWindow);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));
        cancelButton->setGeometry(QRect(280, 230, 75, 23));
        spinBox = new QSpinBox(SocketConfigWindow);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(170, 150, 101, 22));

        retranslateUi(SocketConfigWindow);

        QMetaObject::connectSlotsByName(SocketConfigWindow);
    } // setupUi

    void retranslateUi(QWidget *SocketConfigWindow)
    {
        SocketConfigWindow->setWindowTitle(QApplication::translate("SocketConfigWindow", "SocketConfigWindow", 0));
        label->setText(QApplication::translate("SocketConfigWindow", "Ip Address", 0));
        label_2->setText(QApplication::translate("SocketConfigWindow", "Port", 0));
        OkButton->setText(QApplication::translate("SocketConfigWindow", "Ok", 0));
        cancelButton->setText(QApplication::translate("SocketConfigWindow", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class SocketConfigWindow: public Ui_SocketConfigWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOCKETCONFIGWINDOW_H
