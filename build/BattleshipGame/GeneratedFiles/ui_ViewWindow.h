/********************************************************************************
** Form generated from reading UI file 'ViewWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWWINDOW_H
#define UI_VIEWWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include "battleshipwidget.h"

QT_BEGIN_NAMESPACE

class Ui_ViewWindow
{
public:
    QFrame *frame;
    BattleshipWidget *m_firstPlayerField;
    BattleshipWidget *m_secondPlayerField;
    QPushButton *pushButton;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *fpPositionLabel;
    QLabel *fpShotResult;
    QLabel *spPositionLabel;
    QLabel *spShotResult;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *ViewWindow)
    {
        if (ViewWindow->objectName().isEmpty())
            ViewWindow->setObjectName(QStringLiteral("ViewWindow"));
        ViewWindow->resize(920, 541);
        frame = new QFrame(ViewWindow);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        m_firstPlayerField = new BattleshipWidget(frame);
        m_firstPlayerField->setObjectName(QStringLiteral("m_firstPlayerField"));
        m_firstPlayerField->setGeometry(QRect(30, 60, 331, 291));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        m_firstPlayerField->setPalette(palette);
        m_secondPlayerField = new BattleshipWidget(frame);
        m_secondPlayerField->setObjectName(QStringLiteral("m_secondPlayerField"));
        m_secondPlayerField->setGeometry(QRect(420, 60, 331, 291));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush);
        m_secondPlayerField->setPalette(palette1);
        pushButton = new QPushButton(frame);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(810, 450, 91, 41));
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(130, 10, 131, 41));
        QFont font;
        font.setPointSize(24);
        label->setFont(font);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(510, 10, 171, 41));
        label_2->setFont(font);
        label_3 = new QLabel(frame);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 380, 141, 31));
        QFont font1;
        font1.setPointSize(18);
        label_3->setFont(font1);
        label_4 = new QLabel(frame);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 420, 141, 31));
        label_4->setFont(font1);
        label_5 = new QLabel(frame);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(440, 420, 141, 31));
        label_5->setFont(font1);
        label_6 = new QLabel(frame);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(440, 380, 141, 31));
        label_6->setFont(font1);
        fpPositionLabel = new QLabel(frame);
        fpPositionLabel->setObjectName(QStringLiteral("fpPositionLabel"));
        fpPositionLabel->setGeometry(QRect(190, 380, 141, 31));
        fpPositionLabel->setFont(font1);
        fpShotResult = new QLabel(frame);
        fpShotResult->setObjectName(QStringLiteral("fpShotResult"));
        fpShotResult->setGeometry(QRect(190, 420, 141, 31));
        fpShotResult->setFont(font1);
        spPositionLabel = new QLabel(frame);
        spPositionLabel->setObjectName(QStringLiteral("spPositionLabel"));
        spPositionLabel->setGeometry(QRect(600, 380, 141, 31));
        spPositionLabel->setFont(font1);
        spShotResult = new QLabel(frame);
        spShotResult->setObjectName(QStringLiteral("spShotResult"));
        spShotResult->setGeometry(QRect(600, 420, 141, 31));
        spShotResult->setFont(font1);
        ViewWindow->setCentralWidget(frame);
        menuBar = new QMenuBar(ViewWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 920, 21));
        ViewWindow->setMenuBar(menuBar);

        retranslateUi(ViewWindow);

        QMetaObject::connectSlotsByName(ViewWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ViewWindow)
    {
        ViewWindow->setWindowTitle(QApplication::translate("ViewWindow", "ViewWindow", 0));
        pushButton->setText(QApplication::translate("ViewWindow", "Shoot", 0));
        label->setText(QApplication::translate("ViewWindow", "Your field", 0));
        label_2->setText(QApplication::translate("ViewWindow", "Enemy field", 0));
        label_3->setText(QApplication::translate("ViewWindow", "Shot position", 0));
        label_4->setText(QApplication::translate("ViewWindow", "Shot result", 0));
        label_5->setText(QApplication::translate("ViewWindow", "Shot result", 0));
        label_6->setText(QApplication::translate("ViewWindow", "Shot position", 0));
        fpPositionLabel->setText(QApplication::translate("ViewWindow", "Shot position", 0));
        fpShotResult->setText(QApplication::translate("ViewWindow", "Shot position", 0));
        spPositionLabel->setText(QApplication::translate("ViewWindow", "Shot position", 0));
        spShotResult->setText(QApplication::translate("ViewWindow", "Shot position", 0));
    } // retranslateUi

};

namespace Ui {
    class ViewWindow: public Ui_ViewWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWWINDOW_H
