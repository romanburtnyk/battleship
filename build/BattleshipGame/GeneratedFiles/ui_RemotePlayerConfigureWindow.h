/********************************************************************************
** Form generated from reading UI file 'RemotePlayerConfigureWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REMOTEPLAYERCONFIGUREWINDOW_H
#define UI_REMOTEPLAYERCONFIGUREWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RemotePlayerConfigureWindow
{
public:
    QLabel *label;
    QPushButton *cancelButton;

    void setupUi(QWidget *RemotePlayerConfigureWindow)
    {
        if (RemotePlayerConfigureWindow->objectName().isEmpty())
            RemotePlayerConfigureWindow->setObjectName(QStringLiteral("RemotePlayerConfigureWindow"));
        RemotePlayerConfigureWindow->resize(557, 256);
        label = new QLabel(RemotePlayerConfigureWindow);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 90, 291, 61));
        QFont font;
        font.setPointSize(25);
        label->setFont(font);
        cancelButton = new QPushButton(RemotePlayerConfigureWindow);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));
        cancelButton->setGeometry(QRect(450, 210, 75, 23));

        retranslateUi(RemotePlayerConfigureWindow);

        QMetaObject::connectSlotsByName(RemotePlayerConfigureWindow);
    } // setupUi

    void retranslateUi(QWidget *RemotePlayerConfigureWindow)
    {
        RemotePlayerConfigureWindow->setWindowTitle(QApplication::translate("RemotePlayerConfigureWindow", "RemotePlayerConfigureWindow", 0));
        label->setText(QApplication::translate("RemotePlayerConfigureWindow", "Wait for game start", 0));
        cancelButton->setText(QApplication::translate("RemotePlayerConfigureWindow", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class RemotePlayerConfigureWindow: public Ui_RemotePlayerConfigureWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REMOTEPLAYERCONFIGUREWINDOW_H
